package com.zqq.loveclick;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Random;

/**
 * @author zqq on 2018/10/17
 */
public class Love extends RelativeLayout {

    private Context mContext;
    float[] num = {-30, -20, 0, 20, 30};//随机心形图片角度

    public Love(Context context) {
        super(context);
        initView(context);
    }

    public Love(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public Love(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        mContext = context;
    }

    /**
     * 1.onMesure()--测量
     * 2.onLayout()--布局
     * 3.onDraw()--绘制
     *  3.1 dispatchDraw()主要是分发给子组件进行绘制（onDraw包含了此方法）
     *      值得注意的是ViewGroup容器组件的绘制，当它没有背景时直接调用的是dispatchDraw()方法, 而绕过了draw()方法
     */
    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        ImageView imageView = new ImageView(mContext);
        LayoutParams params = new LayoutParams(80, 80);
        params.leftMargin = getWidth() - 200;
        params.topMargin = getHeight() / 2 - 300;

        imageView.setImageDrawable(getResources().getDrawable(R.mipmap.heart_arrow));
        imageView.setLayoutParams(params);
        addView(imageView);

        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(mContext, "这里是点击爱心的动画，待展示", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * onTouch() > onTouchEvent() > onClick()
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final ImageView imageView = new ImageView(mContext);
        LayoutParams params = new LayoutParams(80, 80);
        params.leftMargin = (int) event.getX() - 40;
        params.topMargin = (int) event.getY() - 80;

        int type = new Random().nextInt(4);
        if (type < 1) {
            imageView.setImageDrawable(getResources().getDrawable(R.mipmap.heart_red));
        } else if (type < 2){
            imageView.setImageDrawable(getResources().getDrawable(R.mipmap.heart_two));
        } else if (type < 3) {
            imageView.setImageDrawable(getResources().getDrawable(R.mipmap.heart_double));
        } else {
            imageView.setImageDrawable(getResources().getDrawable(R.mipmap.heart_dou));
        }
        imageView.setLayoutParams(params);
        addView(imageView);

        AnimatorSet animatorSet=new AnimatorSet();
        animatorSet.play(scale(imageView, "scaleX", 2f, 0.9f, 500, 0))  //缩放动画，X轴2倍缩小至0.9倍
                .with(scale(imageView, "scaleY", 2f, 0.9f, 500, 0))     //缩放动画，Y轴2倍缩小至0.9倍
                .with(rotation(imageView, 0, 0, num[new Random().nextInt(4)]))          //旋转动画，随机旋转角度num={-30.-20，0，20，30}
                .with(alpha(imageView, 0, 1, 100, 0))           //渐变透明度动画，透明度从0-1.
                .with(scale(imageView, "scaleX", 0.9f, 1, 50, 150)) //缩放动画，X轴0.9倍缩小至1倍
                .with(scale(imageView, "scaleY", 0.9f, 1, 50, 150)) //缩放动画，Y轴0.9倍缩小至1倍
                .with(translationY(imageView, 0, -600, 1000, 400))           //平移动画，Y轴从0向上移动600单位
                .with(alpha(imageView, 1, 0, 300, 400))         //透明度动画，从1-0
                .with(scale(imageView, "scaleX", 1, 3f, 700, 400))  //缩放动画，X轴1倍放大至3倍
                .with(scale(imageView, "scaleY", 1, 3f, 700, 400)); //缩放动画，Y轴1倍放大至3倍

        animatorSet.start();
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                removeViewInLayout(imageView);
            }
        });

        return super.onTouchEvent(event);
    }

    public static ObjectAnimator scale(View view, String propertyName, float from, float to, long time, long delayTime) {
        ObjectAnimator translation = ObjectAnimator.ofFloat(view, propertyName, from, to);
        translation.setInterpolator(new LinearInterpolator());
        translation.setStartDelay(delayTime);
        translation.setDuration(time);
        return translation;
    }

    public static ObjectAnimator rotation(View view, long time, long delayTime, float... values) {
        ObjectAnimator rotation = ObjectAnimator.ofFloat(view, "rotation", values);
        rotation.setDuration(time);
        rotation.setStartDelay(delayTime);
        rotation.setInterpolator(new TimeInterpolator() {
            @Override
            public float getInterpolation(float input) {
                return input;
            }
        });
        return rotation;
    }

    public static ObjectAnimator alpha(View view, float from, float to, long time, long delayTime) {
        ObjectAnimator translation = ObjectAnimator.ofFloat(view, "alpha", from, to);
        translation.setInterpolator(new LinearInterpolator());
        translation.setStartDelay(delayTime);
        translation.setDuration(time);
        return translation;
    }

    public static ObjectAnimator translationY(View view, float from, float to, long time, long delayTime) {
        ObjectAnimator translation = ObjectAnimator.ofFloat(from, "translationY", from, to);
        translation.setInterpolator(new LinearInterpolator());
        translation.setStartDelay(delayTime);
        translation.setDuration(time);
        return translation;
    }

    public static ObjectAnimator translationX(View view, float from, float to, long time, long delayTime) {
        ObjectAnimator translation = ObjectAnimator.ofFloat(view, "translationX", from, to);
        translation.setInterpolator(new LinearInterpolator());
        translation.setStartDelay(delayTime);
        translation.setDuration(time);
        return translation;
    }


}
