package com.zxx.myclock

import android.app.Application

/**
 * @author zxx on 2019/7/25
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        MyApplication.context = applicationContext as Application
    }

    companion object {
        lateinit var context: Application
    }

}