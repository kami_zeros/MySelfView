package com.zxx.myclock

import android.app.Activity
import android.app.WallpaperManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.zxx.myclock.MyApplication.Companion.context
import com.zxx.myclock.widget.TextClockWallpaperService
import kotlinx.android.synthetic.main.activity_clock.*
import java.io.Serializable
import java.security.AccessControlContext
import java.util.*
import kotlin.concurrent.timer

class ContainerActivity : AppCompatActivity() {

    private lateinit var mArgParams: ActivityParams
    private lateinit var mTypeMap: MutableMap<String, () -> Unit>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mTypeMap = mutableMapOf(
                TYPE_TEXT_CLOCK to { caseTextClock() }
//                TYPE_SHADOW_LAYOUT to { caseShadowLayout() },
//                TYPE_RADAR_VIEW to { caseRadarView() }
        )

        //处理参数数据
        mArgParams = if (savedInstanceState != null) {
            savedInstanceState.getSerializable(ARG_PARAMS) as ActivityParams
        } else {
            intent.getSerializableExtra(ARG_PARAMS) as ActivityParams
        }

        mTypeMap[mArgParams.showType]?.invoke()
    }

    /**
     * 保存参数数据
     */
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        if (::mArgParams.isInitialized) {
            outState?.putSerializable(ARG_PARAMS, mArgParams)
        }
    }

    //----------------------------------
    //* 文字时钟
    //----------------------------------
    private var mTimer: Timer? = null

    private fun caseTextClock() {
        setContentView(R.layout.activity_clock)
        mTimer = timer(period = 1000) {
            runOnUiThread {
                textClock.doInvalidate()
            }
        }

        //设置壁纸，API至少是16
        btnSet.setOnClickListener {
            val intent = Intent().apply {
                action = WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER
                putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, ComponentName(
                        applicationContext, TextClockWallpaperService::class.java
                ))
            }
            startActivity(intent)
        }

        //还原壁纸
        btnClear.setOnClickListener {
            WallpaperManager.getInstance(this).clear()
        }

    }


    override fun onDestroy() {
        super.onDestroy()
        mTimer?.cancel()
    }

    companion object {
        private const val ARG_PARAMS = "ARG_PARAMS"
        const val TYPE_TEXT_CLOCK = "TYPE_TEXT_CLOCK"
        const val TYPE_SHADOW_LAYOUT = "TYPE_SHADOW_LAYOUT"
        const val TYPE_RADAR_VIEW = "TYPE_RADAR_VIEW"

        @JvmStatic
        fun navigate(context: Context, params: ActivityParams) {
            val intent = Intent(context, ContainerActivity::class.java).apply {
                if (context !is Activity) {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                }
            }
            intent.putExtra(ARG_PARAMS, params)
            context.startActivity(intent)
        }
    }

    data class ActivityParams(val showType: String) : Serializable

}
