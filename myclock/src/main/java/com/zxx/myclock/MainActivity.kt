package com.zxx.myclock

import android.app.WallpaperManager
import android.content.ComponentName
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.zxx.myclock.widget.TextClockWallpaperService
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.concurrent.timer

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)

        //旋转时钟
//        timeClock.setOnClickListener {
//            ContainerActivity.navigate(
//                    this,
//                    ContainerActivity.ActivityParams(ContainerActivity.TYPE_TEXT_CLOCK))
//        }

        val intent = Intent().apply {
            action = WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER
            putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, ComponentName(
                    applicationContext, TextClockWallpaperService::class.java
            ))
        }
        startActivity(intent)
    }

}
