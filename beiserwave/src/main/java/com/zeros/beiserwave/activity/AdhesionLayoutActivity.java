package com.zeros.beiserwave.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.zeros.beiserwave.R;
import com.zeros.beiserwave.widget.adhesion.AdhesionLayout;

public class AdhesionLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adhesion_layout);

        AdhesionLayout adhesionLayout = (AdhesionLayout) findViewById(R.id.adhesion);

        adhesionLayout.setOnAdherentListener(new AdhesionLayout.OnAdherentListener() {
            @Override
            public void onDismiss() {
                Toast.makeText(AdhesionLayoutActivity.this, "消失", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
