package com.zeros.beiserwave.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.zeros.beiserwave.R;
import com.zeros.beiserwave.widget.wave.WaveView;

public class WaveActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wave);

        WaveView waveView = (WaveView) findViewById(R.id.wave);
        waveView.setRunning();
    }
}
