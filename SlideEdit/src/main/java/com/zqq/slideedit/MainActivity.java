package com.zqq.slideedit;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.zqq.slideedit.adapter.InventoryAdapter;
import com.zqq.slideedit.bean.Inventory;
import com.zqq.slideedit.widget.SlideRecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * SlideRecyclerView测试
 */
public class MainActivity extends AppCompatActivity {

    private SlideRecyclerView slideRecyclerView;
    private List<Inventory> mInventories;
    private InventoryAdapter mInventoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
    }

    private void initView() {
        slideRecyclerView = findViewById(R.id.recycler_view_list);
        slideRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider_inset));
        slideRecyclerView.addItemDecoration(itemDecoration);

        mInventories = new ArrayList<>();
        Inventory inventory;
        Random random = new Random();
        for (int i = 0; i < 50; i++) {
            inventory = new Inventory();
            inventory.setItemDesc("测试数据" + i);
            inventory.setQuantity(random.nextInt(100000));
            inventory.setItemCode("0120816");
            inventory.setDate("20180219");
            inventory.setVolume(random.nextFloat());
            mInventories.add(inventory);
        }

        mInventoryAdapter = new InventoryAdapter(this, mInventories);
        slideRecyclerView.setAdapter(mInventoryAdapter);

        mInventoryAdapter.setOnDeleteClickListener(new InventoryAdapter.OnDeleteClickListener() {
            @Override
            public void onDeleteClick(View view, int position) {
                mInventories.remove(position);
                mInventoryAdapter.notifyDataSetChanged();
                slideRecyclerView.closeMenu();
            }
        });
    }
}
