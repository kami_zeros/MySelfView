package com.zqq.slideedit.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.zqq.slideedit.R;
import com.zqq.slideedit.bean.Inventory;

import java.util.List;

/**
 * 清单列表adapter
 * 只能有一个删除
 * @author zqq on 2018/10/31
 */
public class InventoryAdapter extends BaseRecyclerViewAdapter<Inventory> {

    private OnDeleteClickListener mDeleteClickListener;

    public InventoryAdapter(Context mContext, List<Inventory> mData) {
        super(mContext, mData, R.layout.item_inventroy);
    }

    public void setOnDeleteClickListener(OnDeleteClickListener mDeleteClickListener) {
        this.mDeleteClickListener = mDeleteClickListener;
    }


    @Override
    protected void onBindData(RecyclerViewHolder holder, Inventory bean, int position) {
        View view = holder.getView(R.id.tv_delete);
        view.setTag(position);
        if (!view.hasOnClickListeners()) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mDeleteClickListener != null) {
                        mDeleteClickListener.onDeleteClick(view, (Integer) view.getTag());
                    }
                }
            });
        }

        ((TextView) holder.getView(R.id.tv_item_desc)).setText(bean.getItemDesc());
        String quantity = bean.getQuantity() + "箱";
        ((TextView) holder.getView(R.id.tv_quantity)).setText(quantity);
        String detail = bean.getItemCode() + "/" + bean.getDate();
        ((TextView) holder.getView(R.id.tv_detail)).setText(detail);
        String volume = bean.getVolume() + "方";
        ((TextView) holder.getView(R.id.tv_volume)).setText(volume);
    }

    public interface OnDeleteClickListener {
        void onDeleteClick(View view, int position);
    }

}
