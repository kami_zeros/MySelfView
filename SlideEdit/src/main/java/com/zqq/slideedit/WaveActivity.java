package com.zqq.slideedit;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.zqq.slideedit.widget.WaveProgressView;

/**
 * 测试波纹进度条
 */
public class WaveActivity extends AppCompatActivity {

    private WaveProgressView wpv_git;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wave);

        wpv_git = findViewById(R.id.wpv_git);

        handler.sendEmptyMessage(0);
//        handler.sendEmptyMessageDelayed(0, 200);
    }


    @SuppressLint("HandlerLeak")
    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            float progress = wpv_git.getProgress();
            if (progress >= 100) {
//                progress = 0;
                wpv_git.setText(progress + "%");
//                handler = null;
                return;
            }
            wpv_git.setProgress(++progress);
            wpv_git.setText(progress + "%");
            handler.sendEmptyMessageDelayed(0, 200);
        }
    };

}
