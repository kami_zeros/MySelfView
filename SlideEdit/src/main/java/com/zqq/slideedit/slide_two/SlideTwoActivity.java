package com.zqq.slideedit.slide_two;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;

import com.zqq.slideedit.R;

public class SlideTwoActivity extends AppCompatActivity {

    private SwipeMenuRecyclerView smrv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_two);

        smrv = findViewById(R.id.recycler);
        smrv.setLayoutManager(new LinearLayoutManager(this));

        smrv.setAdapter(new RecyclerAdapter(this));
        smrv.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_see_github, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.option_see_github:
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://github.com/freeze-frame/SwipeMenuRecyclerView")));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
