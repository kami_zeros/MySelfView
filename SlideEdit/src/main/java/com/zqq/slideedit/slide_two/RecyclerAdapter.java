package com.zqq.slideedit.slide_two;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.zqq.slideedit.MainActivity;
import com.zqq.slideedit.R;

/**
 * @author zqq on 2018/10/31
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;

    public RecyclerAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mText.setText("ItemView " + position);

        holder.mText.setTag(position);
        holder.mRenameButton.setTag(position);
        holder.mDeleteButton.setTag(position);
        holder.mTopButton.setTag(position);
    }

    @Override
    public int getItemCount() {
        return 20;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text:
                Toast.makeText(context, "Click itemView " + v.getTag().toString(),
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.button_resetName:
                Toast.makeText(context, "Rename itemView " + v.getTag().toString(),
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.button_delete:
                Toast.makeText(context, "Delete itemView " + v.getTag().toString(),
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.button_top:
                Toast.makeText(context, "Top itemView " + v.getTag().toString(),
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mText;
        final TextView mRenameButton;
        final TextView mDeleteButton;
        final TextView mTopButton;

        private ViewHolder(View itemView) {
            super(itemView);
            mText = itemView.findViewById(R.id.text);
            mRenameButton = itemView.findViewById(R.id.button_resetName);
            mDeleteButton = itemView.findViewById(R.id.button_delete);
            mTopButton = itemView.findViewById(R.id.button_top);

            mText.setOnClickListener(RecyclerAdapter.this);
            mRenameButton.setOnClickListener(RecyclerAdapter.this);
            mDeleteButton.setOnClickListener(RecyclerAdapter.this);
            mTopButton.setOnClickListener(RecyclerAdapter.this);
        }
    }

}
