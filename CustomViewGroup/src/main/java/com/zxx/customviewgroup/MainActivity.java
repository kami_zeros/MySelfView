package com.zxx.customviewgroup;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;


public class MainActivity extends AppCompatActivity {
    int screenWidth;
    LinearLayout rootView;

    private DragFlowLayout dragFlowLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rootView = findViewById(R.id.activity_main);
        dragFlowLayout = findViewById(R.id.dfl);

        addClassify();

        addDragView();
    }

    private void addDragView() {
        for (int i = 0; i < 25; i++) {
            TextView textView = new TextView(this);
            textView.setText(i + "添加");
            textView.setPadding(10, 12, 12, 12);
            dragFlowLayout.addView(textView);
        }
    }

    private void addClassify() {
        TextView tvTitle = new TextView(this);
        tvTitle.setGravity(Gravity.CENTER);
        tvTitle.setTextColor(Color.RED);
        tvTitle.setTextSize(30);
        tvTitle.setText("汽车和提醒");
        rootView.addView(tvTitle, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 80));

        CustomFlowLayout customFlowLayout = new CustomFlowLayout(this);
        customFlowLayout.setLineSpacing(0);
        customFlowLayout.setPadding(customFlowLayout.dip2px(8), 0, customFlowLayout.dip2px(8), 0);
        rootView.addView(customFlowLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        String[] imgUrls = getResources().getStringArray(R.array.imageUrl);
        screenWidth = this.getResources().getDisplayMetrics().widthPixels - customFlowLayout.dip2px(8) * 2;
        for (int i = 0; i < imgUrls.length; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setAdjustViewBounds(true);
            Glide.with(this)
                    .load(imgUrls[i])
                    .into(imageView);

            customFlowLayout.addView(imageView, new ViewGroup.MarginLayoutParams(screenWidth / 4, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

        customFlowLayout.setClickListener(new CustomFlowLayout.OnItemClickListener() {
            @Override
            public void imageClick(Object data, int pos) {
                Log.e("Tag-", data != null ? data.toString() : "data为null");
                Toast.makeText(MainActivity.this, "pos==>" + pos, Toast.LENGTH_SHORT).show();
            }
        });
    }


}
