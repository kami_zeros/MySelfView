package com.zeros.myself;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.zeros.myself.practice_delete.PageFragment;

import com.zeros.myself.practice_delete.practice2.Practice2Activity;
import com.zeros.myself.practice_delete.practice3.Practice3Activity;
import com.zeros.myself.practice_delete.practice4.Practice4Activity;
import com.zeros.myself.practice_delete.practice5.Practice5Activity;
import com.zeros.myself.practice_delete.practice6.Practice6Activity;
import com.zeros.myself.practice_delete.practice7.Practice7Activity;
import com.zeros.myself.selfview.KnockLoadingView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private KnockLoadingView loadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadingView = findViewById(R.id.zhongbai);
        click_stop(loadingView);

        initPager();
    }

    //开启动画 按钮
    public void click_start(View view) {
        if (loadingView != null) {
            loadingView.startAnima();
        }
    }

    public void click_stop(View view) {
        if (loadingView != null) {
            loadingView.stopAnimation();
        }
    }


    //跳转到Practice2界面（自定义view练习-Paint）
    public void jumpToPractice2(View view) {
        Intent intent = new Intent(this, Practice2Activity.class);
        startActivity(intent);
        click_stop(view);
    }


    //跳转到Practice3界面（自定义view练习-drawText）
    public void jumpToPractice3(View view) {
        Intent intent = new Intent(this, Practice3Activity.class);
        startActivity(intent);
        click_stop(view);
    }

    //跳转到Practice4界面（自定义view练习-Canvas对绘制的辅助）
    //clipXXX() ：范围裁切  和  Matrix：集合变换
    public void jumpToPractice4(View view) {
        Intent intent = new Intent(this, Practice4Activity.class);
        startActivity(intent);
        click_stop(view);
    }

    //跳转到Practice5界面（绘制顺序）
    public void jumpToPractice5(View view) {
        Intent intent = new Intent(this, Practice5Activity.class);
        startActivity(intent);
        click_stop(view);
    }

    public void jumpToPractice6(View view) {
        jumpToActivity(view, Practice6Activity.class);
    }

    public void jumpToPractice7(View view) {
        jumpToActivity(view, Practice7Activity.class);
    }


    private void jumpToActivity(View view, Class clazz) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
        click_stop(view);
    }


    //    ---------------------以下是View pager处理-------------------
    private List<PageModel> pageModels = new ArrayList<>();

    private void initPager() {
        ViewPager pager = findViewById(R.id.pager);
        pager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                PageModel pageModel = pageModels.get(position);
                return PageFragment.newInstance(pageModel.sampleLayoutRes, pageModel.practiceLayoutRes);
            }

            @Override
            public int getCount() {
                return pageModels.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return getString(pageModels.get(position).titleRes);
            }
        });

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(pager);
    }

    {
        pageModels.add(new PageModel(R.layout.sample_color, R.string.title_draw_color, R.layout.practice_color));
        pageModels.add(new PageModel(R.layout.sample_circle, R.string.title_draw_circle, R.layout.practice_circle));
        pageModels.add(new PageModel(R.layout.sample_rect, R.string.title_draw_rect, R.layout.practice_rect));
        pageModels.add(new PageModel(R.layout.sample_point, R.string.title_draw_point, R.layout.practice_point));
        pageModels.add(new PageModel(R.layout.sample_oval, R.string.title_draw_oval, R.layout.practice_oval));
        pageModels.add(new PageModel(R.layout.sample_line, R.string.title_draw_line, R.layout.practice_line));
        pageModels.add(new PageModel(R.layout.sample_round_rect, R.string.title_draw_round_rect, R.layout.practice_round_rect));
        pageModels.add(new PageModel(R.layout.sample_arc, R.string.title_draw_arc, R.layout.practice_arc));
        pageModels.add(new PageModel(R.layout.sample_path, R.string.title_draw_path, R.layout.practice_path));
        pageModels.add(new PageModel(R.layout.sample_histogram, R.string.title_draw_histogram, R.layout.practice_histogram));
        pageModels.add(new PageModel(R.layout.sample_pie_chart, R.string.title_draw_pie_chart, R.layout.practice_pie_chart));
    }


    private class PageModel {
        @LayoutRes
        int sampleLayoutRes;
        @StringRes
        int titleRes;
        @LayoutRes
        int practiceLayoutRes;

        PageModel(@LayoutRes int sampleLayoutRes, @StringRes int titleRes, @LayoutRes int practiceLayoutRes) {
            this.sampleLayoutRes = sampleLayoutRes;
            this.titleRes = titleRes;
            this.practiceLayoutRes = practiceLayoutRes;
        }
    }


}
