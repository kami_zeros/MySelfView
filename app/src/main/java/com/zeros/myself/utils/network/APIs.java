package com.zeros.myself.utils.network;

//import com.yunbao.financial.model.OssInfo;
//import com.yunbao.financial.model.Version;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * 网络接口
 */

public interface APIs {

    /**
     * 1. 可以通过该方法请求任何一个接口
     *
     * @param data 请求数据
     * @return 被观察者
     */
    @FormUrlEncoded
    @POST("{type}")
    Observable<Object> uncertainAPI(@Path("type") String type,
                                    @FieldMap Map<String, String> data);

    /**
     * 可以通过该方法请求任何一个接口
     * 使用@Field时记得添加@FormUrlEncoded
     * @param url  若需要重新定义接口地址，可以使用@Url，将地址以参数的形式传入即可
     */
    @FormUrlEncoded
    @POST
    Observable<Object> uncertainAPI2(@Url String url,
                                     @FieldMap Map<String, String> data);


    /**
     * 2. 更新版本get_domain
     */
    @GET("/api/common/get_domain")
    Observable<Object> testDo_main();


    /**
     * 2.1 更新版本get_domain
     */
//    @POST("/sysver/domain")
//    Observable<Version> doMain();


    /**
     * 3. 获取token参数
     */
    @POST("/api/user/get_token")
    Observable<String> requestToken();

    /**
     * 4. 获取token参数
     */
    @POST("/api/my_invest/detail")
    Observable<String> myInvest();


    /**
     * 5. 获取Oss参数
     * @param url  若需要重新定义接口地址，可以使用@Url，将地址以参数的形式传入即可
     */
//    @GET
//    Observable<OssInfo> getOssInfo(@Url String url);


}
