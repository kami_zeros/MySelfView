//package com.zeros.myself.utils.http.interceptor;
//
//import android.text.TextUtils;
//
//import com.zeros.mysplash.UserCenter;
//
//import java.io.IOException;
//
//import okhttp3.Headers;
//import okhttp3.HttpUrl;
//import okhttp3.Interceptor;
//import okhttp3.Request;
//import okhttp3.Response;
//
///**
// * Created by Administrator on 2017/12/21.
// */
//
//public class AddQueryParameterInterceptor implements Interceptor {
//    @Override
//    public Response intercept(Chain chain) throws IOException {
//        Request originalRequest = chain.request();
////        String method = originalRequest.method();
////        Headers headers = originalRequest.headers();
//
//        String token = UserCenter.getInstance().getToken();
//        String uid = String.valueOf(UserCenter.getInstance().getCurrentUser().getUid());
//
//        HttpUrl modifiedUrl = originalRequest.url().newBuilder()
//                .addQueryParameter("token", TextUtils.isEmpty(UserCenter.getInstance().getToken()) ? "" : token)
//                .addQueryParameter("uid", UserCenter.getInstance().getCurrentUser().getUid() == 0 ? "" : uid)
//                .build();
//
//        Request request = originalRequest.newBuilder()
//                .url(modifiedUrl)
//                .build();
//        return chain.proceed(request);
//    }
//}
