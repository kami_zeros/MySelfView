package com.zeros.myself.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/**
 * 单位转换工具 on 2017/4/18.
 */

public class DensityUtil {

    /**
     * 1.根据手机的分辨率从 dp 的单位 转成为 px(像素)，保证尺寸大小不变
     * @param dpValue （DisplayMetrics类中属性density）
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 2.根据手机的分辨率从 px(像素) 的单位 转成为 dp，保证尺寸大小不变
     * @param pxValue （DisplayMetrics类中属性density）
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }


    /**
     * 3.将px值转换为sp值，保证文字大小不变
     * @param pxValue （DisplayMetrics类中属性scaledDensity）
     */
    public static float px2sp(Context context, float pxValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (pxValue / fontScale + 0.5f);
    }


    /**
     * 4.将sp值转换为px值，保证文字大小不变
     * @param spValue （DisplayMetrics类中属性scaledDensity）
     */
    public static float sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (spValue * fontScale + 0.5f);
    }
	
	/**
     * @Description: 获得屏幕高度
     */
    public static int getHeight(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    /**
     * @Description: 获得屏幕宽度
     */
    public static int getWidth(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    /**
     * @Description: 获得屏幕宽度
     */
    public static int getWidth(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }
	

}
