package com.zeros.myself.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * 时间日期格式化 on 2017/6/3.
 */

public class DateFormat {
	
	 /**
     * 0. 获取当前时间
     */
    public static long getDate(){
        long time = System.currentTimeMillis();
        return time;
    }

    /**
     * 0.0 获取实时网络时间
     */
    public static String getNetDate(){
        SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dff.setTimeZone(TimeZone.getTimeZone("GMT+08"));        //获取实时网络时间

        String timeStr = dateToStamp(dff.format(new Date()));   //转换为时间戳
//        long time = Long.parseLong(timeStr);    //long型

        return timeStr;
    }

    /**
     * 1.日期格式化
     *
     * @param date 日期
     * @return 日期格式化
     */
    public static String Format(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }

    /**
     * 1.1 长整型 时间戳 -- 格式化
     */
    public static String Format(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(time);
        return sdf.format(date);
    }


    /**
     * 2.字符串时间戳 -转化为- 日期格式化
     * @param stamp 时间戳
     * @return 格式化日期
     */
    public static String Format(String stamp) {
        if (stamp == null) {
            return "";
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        long time = Long.parseLong(stamp);

        return format.format(time * 1000);//format.format(new Date(time));

 //        long now = System.currentTimeMillis();//毫秒
//        Log.e("Now", String.valueOf(now));
//        Log.e("Date", String.valueOf(time*1000));
//        Log.e("FormatNow   ", format.format(now));
//        Log.e("FormatDate   ", format.format(time*1000));
    }


    /**
     * 3.将时间转换为时间戳
     * @param str 时间
     * @return 时间戳
     */
    public static String dateToStamp(String str){
        String stamp = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = format.parse(str);
            long ts = date.getTime();
            String stampstr = String.valueOf(ts);

            stamp = stampstr.substring(0, 10);//后三位省略
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return stamp;
    }

}
