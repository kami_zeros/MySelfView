//package com.zeros.myself.utils.http.network;
//
//import android.content.Context;
//import android.support.annotation.NonNull;
//
//import com.zeros.myself.utils.http.interceptor.LoggingInterceptor;
//
//import java.io.File;
//import java.util.concurrent.TimeUnit;
//
//import okhttp3.Cache;
//import okhttp3.OkHttpClient;
//import okhttp3.internal.cache.CacheInterceptor;
//import retrofit2.Retrofit;
//import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
//import retrofit2.converter.gson.GsonConverterFactory;
//
///**
// * 全局 单例网络请求Client 请求方法 调用静态方法getInstance().***();
// * 其中*** 为ApiStore中定义的请求方法 on 2017/12/20.
// */
//
//public class HttpClient {
//
//    private static final int CONNECT_TIME_OUT = 3000;
//    private static final int READ_TIME_OUT = 5000;
//    private static final int WRITE_TIME_OUT = 5000;
//
//    private Context context;
//    private static Retrofit retrofit;
//
//    public HttpClient(Context context) {
//        this.context = context;
//    }
//
//    public static ApiStores getInstance() {
//        Retrofit retrofit = getClient();
//        return retrofit.create(ApiStores.class);
//    }
//
//    //Retrofit与OkHttp设置
//    private static Retrofit getClient() {
//        if (retrofit == null) {
//            OkHttpClient.Builder builder = new OkHttpClient.Builder()
//                    //设置超时和重连
//                    .connectTimeout(CONNECT_TIME_OUT, TimeUnit.MILLISECONDS)
//                    .readTimeout(READ_TIME_OUT, TimeUnit.MILLISECONDS)
//                    .writeTimeout(WRITE_TIME_OUT, TimeUnit.MILLISECONDS)
//                    //错误重连
//                    .retryOnConnectionFailure(true);
//            // 缓存设置
//            setCacheConfig(builder);
//            // https设置
//            setHttpsConfig(builder);
//
//            //Log信息拦截器，debug模式下打印log
//            String BASE_URL = setLogConfig(builder);
//
//            OkHttpClient client = builder.build();
//            retrofit = new Retrofit.Builder()
//                    .baseUrl(BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                    .client(client)
//                    .build();
//        }
//        return retrofit;
//    }
//
//
//    @NonNull
//    private static String setLogConfig(OkHttpClient.Builder builder) {
//        String baseUrl;
//        baseUrl = Constants.API_DEBUG_SERVER_URL;
//
//        LoggingInterceptor interceptor = new LoggingInterceptor();
//        interceptor.setLevel(LoggingInterceptor.Level.BODY);
//        builder.addInterceptor(interceptor);
//        return baseUrl;
//    }
//
//
//    //Https设置
//    private static void setHttpsConfig(OkHttpClient.Builder builder) {
//        HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(null, null, null);
//        builder.sslSocketFactory(sslParams.sslSocketFactory, sslParams.trustManager);
//    }
//
//    //缓存设置
//    private static void setCacheConfig(OkHttpClient.Builder builder) {
//        File cacheFile = new File(MyApplication.getContext().getExternalCacheDir(), "ColumnCache");
//        Cache cache = new Cache(cacheFile, 1024 * 1024 * 50);
//        builder.cache(cache)
//                .addInterceptor(new CacheInterceptor());
//    }
//
//
//}
