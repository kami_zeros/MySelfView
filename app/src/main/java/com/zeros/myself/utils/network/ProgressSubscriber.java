package com.zeros.myself.utils.network;

import android.util.Log;

//import com.yunbao.financial.HujinApplication;
//import com.yunbao.financial.common.Config;
//import com.yunbao.financial.utils.tip.ToastUtil;
//import com.yunbao.financial.utils.tip.ZLog;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * 显示进度条的观察者 on 2017/9/27.
 */

public abstract class ProgressSubscriber<T> implements Observer<T>{

    private long startTime;
//    private Consumer<Object> nextAction;        //
//    private Consumer<Throwable> errorAction;    //可以把错误响应方法抛出去
    private boolean needLoadingView = true;
    private boolean showErrorToast = true;      //吐司错误信息
    private boolean hideProgress = true;        //成功加载完成，是否隐藏加载框

//    public ProgressSubscriber(Consumer<Object> nextAction) {
//        this.nextAction = nextAction;
//    }
//
//    public ProgressSubscriber(Consumer<Object> nextAction, Consumer<Throwable> errorAction) {
//        this.nextAction = nextAction;
//        this.errorAction = errorAction;
//    }

    public ProgressSubscriber(boolean showLoading) {
        this.needLoadingView = showLoading;
        initShowLoading();
    }

    public ProgressSubscriber(boolean showLoading, boolean showErrorToast) {
        this.needLoadingView = needLoadingView;
        this.showErrorToast = showErrorToast;
        initShowLoading();
    }

    private void initShowLoading() {
        startTime = System.currentTimeMillis();
        showLoading();
        //其他订阅方法
    }


    //显示LoadingDialog
    private void showLoading() {
        if (!this.needLoadingView) return;
//        HujinApplication.getHujinApp().showLoading();
    }

    private void dismissLoading() {
        if (this.needLoadingView) {
//            HujinApplication.getHujinApp().dismissLoadingDialog();
        }
    }

    /*********************---------观察者方法-----------**********************/
    @Override
    public void onSubscribe(Disposable d) { //Dispose参数是由1.x中的Subscription改名的，为了避免名称冲突
       //可以使用d.dispose()方法来取消订阅
    }

    /**
     * 对错误进行统一处理（若不能统一则继承此方法）
     * 隐藏ProgressDialog
     */
    @Override
    public void onError(Throwable e) {
//        ZLog.e("Tag-ProgressSubscriber-"+"Error");

        if (e == null) { //自定义其他错误

        } else {
//            postRrror(e);
        }
    }

public final static int LOADING_VIEW_DELAYED = 1000;    //加载框延迟取消的时间
    //Action1，在2.x中使用Consumer来代替，如果是两个参数，则用BiConsumer来代替Action2，而且在2.x中删除了Action3-9，如果是多个参数则用Custom<Object[]>代替ActionN
//    private void postRrror(final Throwable e) {
//        long expendTime = System.currentTimeMillis() - startTime;
//        Observable.timer(expendTime >= Config.LOADING_VIEW_DELAYED ? 0 : Config.LOADING_VIEW_DELAYED - expendTime, TimeUnit.MILLISECONDS)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<Object>() {
//                    @Override
//                    public void accept(Object object) throws Exception {
//                        if (showErrorToast) {
//                            if (e instanceof SocketTimeoutException) {
//                                ToastUtil.ToastMsg(HujinApplication.getHujinApp(), "网络中断，请检查您的网络状态", 2000);
//                            } else if (e instanceof ConnectException) {
//                                ToastUtil.ToastMsg(HujinApplication.getHujinApp(), "网络中断，请检查您的网络状态", 2000);
//                            } else {
//                                ZLog.e(e.getMessage());
//                                ToastUtil.ToastMsg(HujinApplication.getHujinApp(), e.getMessage());
//                            }
//                        }
//                        dismissLoading();
////                        if (errorAction != null) {
////                            errorAction.accept(e);
////                        }
//                    }
//                });
//    }

    /**
     * 完成，隐藏ProgressDialog
     */
    @Override
    public void onComplete() {
        Log.e("Tag-Progress-", "complete");
        this.dismissLoading();
    }

    /**
     * 将onNext方法中的返回结果交给Activity或Fragment自己处理
     */
//    @Override
//    public void onNext(final T t) {
//        long expendTime = System.currentTimeMillis() - startTime;
//        Observable.timer(expendTime >= Config.LOADING_VIEW_DELAYED ? 0 : Config.LOADING_VIEW_DELAYED - expendTime, TimeUnit.MILLISECONDS)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<Object>() {
//                    @Override
//                    public void accept(Object o) throws Exception {
//                        if (hideProgress) {
//                            dismissLoading();
//                        }
//                        if (nextAction != null) {
//                            nextAction.accept(t);
//                        }
//                    }
//                });
//    }

}
