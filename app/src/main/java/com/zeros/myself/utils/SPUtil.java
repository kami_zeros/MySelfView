package com.zeros.myself.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

/**
 * 对偏好设置文件进行操作的工具类 on 2017/9/20.
 */

public class SPUtil {

    //    private static final String APP_VERSION = "app_version";        //app版本号
    public static final String SP_FILE = "hujin";      //偏好设置保存文件名
    public static final String USER_INFO = "userInfo";        //保存用户所有信息json
    public static final String EMPTY = "";

    public SharedPreferences sp;
    public static Editor editor;

    //此处构造方法可以动态设置 --偏好设置保存文件名name
    public SPUtil(Context context, String name) {
        sp = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public SPUtil(Context context) {
        this.sp = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sp.edit();//commit或者apply
    }

    /**
     * 1.存储数据到sharedpreference
     */
    public static void put(Context context, String key, Object object) {
        try {
            SharedPreferences sp = context.getSharedPreferences(SP_FILE, Context.MODE_PRIVATE);
            Editor editor = sp.edit();

            if (object instanceof Integer) {
                editor.putInt(key, (Integer) object);
            } else if (object instanceof Boolean) {
                editor.putBoolean(key, (Boolean) object);
            } else if (object instanceof Float) {
                editor.putFloat(key, (Float) object);
            } else if (object instanceof Long) {
                editor.putLong(key, (Long) object);
            } else {
                editor.putString(key, (String) object);
            }
            editor.apply();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 2.获取SharedPreferences中存储的数据
     * @param object 在读取存储时的默认值（可以写null）
     */
    public static Object get(Context context, String key, Object object) {
        try {
            SharedPreferences sp = context.getSharedPreferences(SP_FILE, Context.MODE_PRIVATE);

            if (object instanceof Integer) {
                return sp.getInt(key, (Integer) object);
            } else if (object instanceof Boolean) {
                return sp.getBoolean(key, (Boolean) object);
            } else if (object instanceof Float) {
                return sp.getFloat(key, (Float) object);
            } else if (object instanceof Long) {
                return sp.getLong(key, (Long) object);
            } else {
                return sp.getString(key, (String) object);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    /**
     * 3.清除sharedpreperence所有数据
     */
    public static void clear(Context context) {
        try {
            SharedPreferences sp = context.getSharedPreferences(SP_FILE, Context.MODE_PRIVATE);
            Editor editor = sp.edit();
            editor.clear();
            editor.apply();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /****************--------自定义存储-------********************/

    /**
     * 4.自定义存储数据到name.xml里
     * @param name 存储的名字
     * @param key key值
     * @param object value值
     */
    public static void putSP(Context context,String name, String key, Object object) {
        try {
            SharedPreferences sp = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            Editor editor = sp.edit();

            if (object instanceof Integer) {
                editor.putInt(key, (Integer) object);
            } else if (object instanceof Boolean) {
                editor.putBoolean(key, (Boolean) object);
            } else if (object instanceof Float) {
                editor.putFloat(key, (Float) object);
            } else if (object instanceof Long) {
                editor.putLong(key, (Long) object);
            } else {
                editor.putString(key, (String) object);
            }
            editor.apply();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 5.获取SharedPreferences中存储的数据
     * @param name 存储的名字（要与putSP对应）
     * @param key key值
     * @param object 在读取存储时的默认值（可以写null）
     */
    public static Object getSP(Context context,String name, String key, Object object) {
        try {
            SharedPreferences sp = context.getSharedPreferences(name, Context.MODE_PRIVATE);

            if (object instanceof Integer) {
                return sp.getInt(key, (Integer) object);
            } else if (object instanceof Boolean) {
                return sp.getBoolean(key, (Boolean) object);
            } else if (object instanceof Float) {
                return sp.getFloat(key, (Float) object);
            } else if (object instanceof Long) {
                return sp.getLong(key, (Long) object);
            } else {
                return sp.getString(key, (String) object);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    /**
     * 6.清除sharedpreperence对应数据
     * @param name
     */
    public static void clearSP(Context context, String name) {
        try {
            SharedPreferences sp = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            Editor editor = sp.edit();
            editor.clear();
            editor.apply();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



}
