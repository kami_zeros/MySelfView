package com.zeros.myself.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import java.io.ByteArrayOutputStream;

/**
 * 图形转化 ▏2018/3/4.
 * 1. 将drawable图片转化成bitmap类型，使用bundle或Intent的extral域直接传递bitmap，这样做，在进入某些app的时候，
 * 程序居然直接崩了，网上百度过后，原来是因为不能直接传递大于40k的图片。
 * 2.把drawable转化为bitmap，再将bitmap存储为byte数组，然后再通过Intent传递。
 */

public class ImageUtil {


    //drawable转化成bitmap的方法
    public static Bitmap drawableToBitamp(Drawable drawable) {
        Bitmap bitmap;

        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();
        System.out.println("Drawable转Bitmap");
        Bitmap.Config config =
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                        : Bitmap.Config.RGB_565;
        bitmap = Bitmap.createBitmap(w, h, config);
        //注意，下面三行代码要用到，否在在View或者surfaceview里的canvas.drawBitmap会看不到图
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, w, h);
        drawable.draw(canvas);

        return bitmap;
    }

    //bitmap转byte
    public static byte[] bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }


    //Glide.with(this).load(url).into(imageView);
    // 加载本地图片
//File file = new File(getExternalCacheDir() + "/image.jpg");
//Glide.with(this).load(file).into(imageView);

// 加载应用资源
//int resource = R.drawable.image;
//Glide.with(this).load(resource).into(imageView);

// 加载二进制流
//byte[] image = getImageBytes();
//Glide.with(this).load(image).into(imageView);

// 加载Uri对象
//Uri imageUri = getImageUri();
//Glide.with(this).load(imageUri).into(imageView);
    //DiskCacheStrategy.NONE参数，这样就可以禁用掉Glide的缓存功能。
    //DiskCacheStrategy.NONE 什么都不缓存
//DiskCacheStrategy.SOURCE 仅仅只缓存原来的全分辨率的图像
//DiskCacheStrategy.RESULT //仅仅缓存最终的图像，即降低分辨率后的（或者是转换后的）
//DiskCacheStrategy.ALL 缓存所有版本的图像（默认行为）

    //圆形图片
//    public class GlideCircleTransform extends BitmapTransformation {
//        public GlideCircleTransform(Context context) {
//            super(context);
//        }
//
//        @Override
//        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
//            return circleCrop(pool, toTransform);
//        }
//
//        private static Bitmap circleCrop(BitmapPool pool, Bitmap source) {
//            if (source == null) return null;
//
//            int size = Math.min(source.getWidth(), source.getHeight());
//            int x = (source.getWidth() - size) / 2;
//            int y = (source.getHeight() - size) / 2;
//
//            // TODO this could be acquired from the pool too
//            Bitmap squared = Bitmap.createBitmap(source, x, y, size, size);
//
//            Bitmap result = pool.get(size, size, Bitmap.Config.ARGB_8888);
//            if (result == null) {
//                result = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
//            }
//
//            Canvas canvas = new Canvas(result);
//            Paint paint = new Paint();
//            paint.setShader(new BitmapShader(squared, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
//            paint.setAntiAlias(true);
//            float r = size / 2f;
//            canvas.drawCircle(r, r, r, paint);
//            return result;
//        }
//
//        @Override
//        public String getId() {
//            return getClass().getName();
//        }
//    }

    //圆角图片
//    public class GlideRoundTransform extends BitmapTransformation {
//        private static float radius = 0f;
//
//        public GlideRoundTransform(Context context) {
//            this(context, 4);
//        }
//
//        public GlideRoundTransform(Context context, int dp) {
//            super(context);
//            this.radius = Resources.getSystem().getDisplayMetrics().density * dp;
//        }
//
//        @Override
//        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
//            return roundCrop(pool, toTransform);
//        }
//
//        private static Bitmap roundCrop(BitmapPool pool, Bitmap source) {
//            if (source == null) return null;
//
//            Bitmap result = pool.get(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
//            if (result == null) {
//                result = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
//            }
//
//            Canvas canvas = new Canvas(result);
//            Paint paint = new Paint();
//            paint.setShader(new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
//            paint.setAntiAlias(true);
//            RectF rectF = new RectF(0f, 0f, source.getWidth(), source.getHeight());
//            canvas.drawRoundRect(rectF, radius, radius, paint);
//            return result;
//        }
//
//        @Override
//        public String getId() {
//            return getClass().getName() + Math.round(radius);
//        }
//    }


}
