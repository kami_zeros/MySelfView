package com.zeros.myself.utils;

import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.widget.Toast;


/**
 * toast 工具类 on 2017/9/19.
 */

public class ToastUtil {

    private static Toast mToast;
    private static Handler mHandler = new Handler();
    private static Runnable runnable = new Runnable() {
        @Override
        public void run() {
            mToast.cancel();
        }
    };


    /**
     * 显示吐司
     * @param text 文本
     * @param duration 时长
     */
    public static void showToast(Context context, String text, int duration) {
        mHandler.removeCallbacks(runnable);
        if (mToast != null) {
            mToast.setText(text);   // 当前token正在显示，直接修改显示的文本
            mToast.setDuration(duration);
        } else {
            mToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
//            mToast.setGravity(Gravity.CENTER, 0, 0);
        }
        mHandler.postDelayed(runnable, duration);
        mToast.show();
    }

    // 默认duration为1000
    public static void ToastMsg(Context mContext, String text) {
        ToastMsg(mContext, text, 1000);
    }


    public static void ToastMsg(Context context, String str, int duration) {
        showToast(context, str, duration);
    }




}
