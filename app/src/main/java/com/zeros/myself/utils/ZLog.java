package com.zeros.myself.utils;

import android.util.Log;

/**
 * Log日志工具类，打印结果形式 类名.方法名(所在行数)：打印的信息 on 2017/11/13.
 */

public class ZLog {

    private static String TAG = "ZLog";     //Application TAG,use "logcat -s TAG"
    private static boolean IS_FULL_CLASSNAME;
    private static int LOG_LEVEL = Log.VERBOSE;

    private static void setIsFullClassname(boolean isFullClassname) {
        ZLog.IS_FULL_CLASSNAME = isFullClassname;
    }

    public static void setLogLevel(int logLevel) {
        //default Log.VERBOSE
        ZLog.LOG_LEVEL = logLevel;
    }

    //0. Tag
    public static void setTAG(String tag) {
        //default "ZLog"
        ZLog.TAG = tag;
    }


    //1. Verbose
    public static void v(String msg) {
        if (LOG_LEVEL <= Log.VERBOSE) {
            Log.v(TAG, getLogTitle() + msg);
        }
    }

    //2. Debug
    public static void d(String msg) {
        if (LOG_LEVEL <= Log.DEBUG) {
            Log.d(TAG, getLogTitle() + msg);
        }
    }

    //3. Info
    public static void i(String msg) {
        if (LOG_LEVEL <= Log.INFO) {
            Log.i(TAG, getLogTitle() + msg);
        }
    }

    //4. Warn
    public static void w(String msg) {
        if (LOG_LEVEL <= Log.WARN) {
            Log.w(TAG, getLogTitle() + msg);
        }
    }

    //5. Error
    public static void e(String msg) {
        if (LOG_LEVEL <= Log.ERROR) {
            Log.e(TAG, getLogTitle() + msg);
        }
    }
	
	 //5.1 Error + Throwable
    public static void e(String msg, Throwable tr) {
        if (LOG_LEVEL <= Log.ERROR) {
            Log.e(TAG, getLogTitle() + msg, tr);
        }
    }

    private static String getLogTitle() {
        StackTraceElement element = Thread.currentThread().getStackTrace()[4];
        String className = element.getClassName();
        if (!IS_FULL_CLASSNAME) {
            int dot = className.lastIndexOf('.');

            if (dot != -1) {
                className = className.substring(dot + 1);
            }
        }
        String methodName = element.getMethodName();
        int lineNumber = element.getLineNumber();

        return className + "." + methodName + "(行:" + lineNumber + ")" + "：";
    }

}
