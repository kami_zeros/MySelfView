package com.zeros.myself.imgcache;

import android.util.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * MD5Encoder加密转String on 2018/1/14.
 */

public class MD5Encoder {
    /**
     * Md5Encoder
     */
    public static String encode(String string) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        byte[] hash = digest.digest(string.getBytes("UTF-8"));

        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10) {
                hex.append("0");
            }
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString();
    }


//    方法二、
    //得到MD5消息摘要，并用BASE64编码成可显示字符串.
//    public static String encrypt(String message){
//        try {
//            MessageDigest md = MessageDigest.getInstance("md5");
//            byte md5[] = md.digest(message.getBytes());
//
//            BASE64Encoder encoder = new BASE64Encoder();
//            return encoder.encode(md5);
//        } catch (NoSuchAlgorithmException e) {
//            throw new RuntimeException(e);
//        }
//    }





}



