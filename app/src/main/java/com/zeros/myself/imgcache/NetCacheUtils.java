package com.zeros.myself.imgcache;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.zeros.myself.utils.ZLog;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 图片的三级缓存分别是：
 * •内存缓存  :优先加载，它速度最快；
 * •本地缓存  :次优先加载，它速度也快；
 * •网络缓存  :速度慢且耗流量。
 * <p>
 * <p>
 * 一、网络缓存
 * •根据图片的url去加载图片
 * •在本地和内存中缓存
 * on 2018/1/14.
 */

public class NetCacheUtils {

    private LocalCacheUtils mLocalCacheUtils;
    private MemoryCacheUtils mMemoryCacheUtils;

    public NetCacheUtils(LocalCacheUtils mLocalCacheUtils, MemoryCacheUtils mMemoryCacheUtils) {
        this.mLocalCacheUtils = mLocalCacheUtils;
        this.mMemoryCacheUtils = mMemoryCacheUtils;
    }

    /**
     * 从网络下载图片
     */
    public void getBitmapFromNet(ImageView ivPic, String url) {
        // 启动AsyncTask,
        // 参数会在doInbackground中获取
        new BitmapTask().execute(ivPic, url);
    }


    /**
     * Handler和线程池的封装
     * <p>
     * 第一个泛型: 参数类型
     * 第二个泛型: 更新进度的泛型,
     * 第三个泛型是onPostExecute的返回结果
     */
    private class BitmapTask extends AsyncTask<Object, Void, Bitmap> {

        private ImageView ivPic;
        private String url;

        /**
         * 后台耗时方法在此执行, 子线程
         */
        @Override
        protected Bitmap doInBackground(Object... params) {
            ivPic = (ImageView) params[0];
            url = (String) params[1];

            ivPic.setTag(url);  // 将url和imageview绑定
            return downloadBitmap(url);
        }

        /**
         * 更新进度, 主线程
         */
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        /**
         * 耗时方法结束后,执行该方法, 主线程
         */
        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                String bindUrl = (String) ivPic.getTag();

                // 确保图片设定给了正确的imageview
                if (url.equals(bindUrl)) {
                    ivPic.setImageBitmap(result);
                    mLocalCacheUtils.setBitmapToLocal(url, result); // 将图片保存在本地
                    mMemoryCacheUtils.setBitmapToMemory(url, result);// 将图片保存在内存
                    System.out.println("从网络缓存读取图片啦...");
                    ZLog.e("从网络缓存读取图片啦...");
                }
            }
        }
    }

    /**
     * 下载图片
     */
    private Bitmap downloadBitmap(String url) {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.setRequestMethod("GET");
            connection.connect();

            int responseCode = connection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = connection.getInputStream();

                //图片压缩处理
                BitmapFactory.Options options = new BitmapFactory.Options();
                //宽高都压缩为原来的二分之一, 此参数需要根据图片要展示的大小来确定
                options.inSampleSize = 2;
                options.inPreferredConfig = Bitmap.Config.RGB_565;  //设置图片格式

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);
                return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

}
