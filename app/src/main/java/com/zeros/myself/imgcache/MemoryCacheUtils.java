package com.zeros.myself.imgcache;

import android.graphics.Bitmap;
import android.util.LruCache;

import java.lang.ref.SoftReference;
import java.util.HashMap;

/**
 * 三、内存缓存 on 2018/1/14.
 * 两个方法：设置内存缓存，获取内存缓存
 * <p>
 * 1.如果使用HashMap存储图片时，当图片越来越多时，会导致内存溢出，
 * 因为它是强引用，java的垃圾回收器不会回收。
 * <p>
 * 2.如若改成软引用SoftReference（内存不够时,垃圾回收器会考虑回收），仍有一个问题：在android2.3+,
 * 系统会优先将SoftReference的对象提前回收掉, 即使内存够用。
 * <p>
 * 3.解决办法：可以用LruCache来解决上述内存不回收或提前回收的问题。
 * least recentlly use 最少最近使用算法 它会将内存控制在一定的大小内, 超出最大值时会自动回收, 这个最大值开发者自己定
 */

public class MemoryCacheUtils {

    //    private HashMap<String, SoftReference<Bitmap>> mMemoryCache = new HashMap<String, SoftReference<Bitmap>>();
    private LruCache<String, Bitmap> mMemoryCache;

    public MemoryCacheUtils() {
        long maxMemory = Runtime.getRuntime().maxMemory() / 8;// 模拟器默认是16M
        mMemoryCache = new LruCache<String, Bitmap>((int) maxMemory){
            @Override
            protected int sizeOf(String key, Bitmap value) {
                // 获取图片占用内存大小
                int byteCount = value.getRowBytes() * value.getHeight();
                return byteCount;
            }
        };
    }

    /**
     * 从内存读
     */
    public Bitmap getBitmapFromMemory(String url){
//        SoftReference<Bitmap> softReference = mMemoryCache.get(url);
//        if (softReference != null) {
//            Bitmap bitmap = softReference.get();
//            return bitmap;
//        }
        return mMemoryCache.get(url);
    }

    /**
     * 写内存
     */
    public void setBitmapToMemory(String url, Bitmap bitmap){
//        SoftReference<Bitmap> softReference = new SoftReference<Bitmap>(bitmap);
//        mMemoryCache.put(url, softReference);
        mMemoryCache.put(url, bitmap);
    }

}
