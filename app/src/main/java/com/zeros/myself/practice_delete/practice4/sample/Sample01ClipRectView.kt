package com.zeros.myself.practice_delete.practice4.sample

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.zeros.myself.R

/**
 * @author zqq on 2020/1/7.
 */
class Sample01ClipRectView : View {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val bitmap: Bitmap

    init {
        bitmap = BitmapFactory.decodeResource(resources, R.mipmap.maps)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        //    clipRect()

        val left = (width - bitmap.width) / 2
        val top = (height - bitmap.height) / 2

        canvas.save()
        canvas.clipRect(left + 50, top + 50, left + 300, top + 200)
        canvas.drawBitmap(bitmap, left.toFloat(), top.toFloat(), paint)

        canvas.restore()
    }

}