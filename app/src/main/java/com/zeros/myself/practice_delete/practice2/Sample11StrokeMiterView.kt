package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/27.
 */
class Sample11StrokeMiterView : View {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val path = Path()

    init {
        //setStrokeMiter(float miter)
        //这个方法是对于 setStrokeJoin() 的一个补充，它用于设置 MITER 型拐角的延长 线的最大值。
        paint.strokeWidth = 40f
        paint.style = Paint.Style.STROKE

        path.rLineTo(200f, 0f)
        path.rLineTo(-160f, 120f)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.save()

        canvas.translate(50f, 100f)
        paint.strokeMiter=1f
        canvas.drawPath(path, paint)

        canvas.translate(300f, 0f)
        paint.strokeMiter = 3f
        canvas.drawPath(path, paint)

        canvas.translate(300f, 0f)
        paint.strokeMiter = 5f
        canvas.drawPath(path, paint)

        canvas.restore()

    }

}