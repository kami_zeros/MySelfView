package com.zeros.myself.practice_delete.practice6.sample;

import android.content.Context;
import android.support.v7.widget.ButtonBarLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.zeros.myself.R;

/**
 * @author zxx on 2020/4/17
 */
public class Sample02Rotation extends RelativeLayout {
    public Sample02Rotation(Context context) {
        super(context);
    }

    public Sample02Rotation(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Sample02Rotation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    Button animateBt;
    ImageView imageView;

    int state = 0;

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        animateBt = findViewById(R.id.animateBt);
        imageView = findViewById(R.id.imageView);

        animateBt.setOnClickListener(v -> {
            switch (state) {
                case 0:
                    imageView.animate().rotation(100);
                    break;
                case 1:
                    imageView.animate().rotation(0);
                    break;
                case 2:
                    imageView.animate().rotationX(180);
                    break;
                case 3:
                    imageView.animate().rotationX(0);
                    break;
                case 4:
                    imageView.animate().rotationY(180);
                    break;
                case 5:
                    imageView.animate().rotationY(0);
                    break;
            }
            state++;
            if (state == 6) {
                state = 0;
            }
        });

    }
}
