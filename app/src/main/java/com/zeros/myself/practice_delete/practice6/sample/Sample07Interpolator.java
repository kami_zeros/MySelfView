package com.zeros.myself.practice_delete.practice6.sample;

import android.content.Context;
import android.graphics.Path;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.support.v4.view.animation.PathInterpolatorCompat;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.zeros.myself.R;
import com.zeros.myself.practice_delete.practice6.DenstityUtil;

import androidx.annotation.Nullable;

/**
 * @author zxx on 2020/4/17
 */
public class Sample07Interpolator extends LinearLayout {

    public Sample07Interpolator(Context context) {
        super(context);
    }

    public Sample07Interpolator(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Sample07Interpolator(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    Spinner spinner;
    Button animateBt;
    ImageView imageView;

    //??
    Interpolator[] interpolators = new Interpolator[13];
    Path path;

    {
        path = new Path();
        path.lineTo(0.25f, 0.25f);
        path.moveTo(0.25f, 1.5f);
        path.lineTo(1, 1);

        interpolators[0] = new AccelerateDecelerateInterpolator();
        interpolators[1] = new LinearInterpolator();
        interpolators[2] = new AccelerateInterpolator();
        interpolators[3] = new DecelerateInterpolator();
        interpolators[4] = new AnticipateInterpolator();
        interpolators[5] = new OvershootInterpolator();
        interpolators[6] = new AnticipateOvershootInterpolator();
        interpolators[7] = new BounceInterpolator();
        interpolators[8] = new CycleInterpolator(0.5f);
        interpolators[9] = PathInterpolatorCompat.create(path);
        interpolators[10] = new FastOutLinearInInterpolator();
        interpolators[11] = new FastOutSlowInInterpolator();
        interpolators[12] = new LinearOutSlowInInterpolator();

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        spinner = findViewById(R.id.interpolatorSpinner);
        animateBt = findViewById(R.id.animateBt);
        imageView = findViewById(R.id.imageView);

        animateBt.setOnClickListener(v -> {
            imageView.animate()
                    .translationX(DenstityUtil.dpToPixel(150))
                    .setDuration(600)
                    .setInterpolator(interpolators[spinner.getSelectedItemPosition()])
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            imageView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    imageView.setTranslationX(0);
                                }
                            }, 500);
                        }
                    });
        });
    }
}
