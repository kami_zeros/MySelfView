package com.zeros.myself.practice_delete;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author zxx on 2019/12/11
 */
public class Practice2DrawCircleView extends View {

    Paint paint = new Paint();


    public Practice2DrawCircleView(Context context) {
        super(context);
    }

    public Practice2DrawCircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Practice2DrawCircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        练习内容：使用 canvas.drawCircle() 方法画圆
//        一共四个圆：1. 实心圆 2. 空心圆 3. 蓝色实心圆 4. 线宽为 20 的空心圆
        //1. Paint.setColor(int color)
        //2.Paint.setStyle(Paint.Style style)
        //3.Paint.setStrokeWidth(float width)
        //4.setAntiAlias(boolean aa)

        paint.setAntiAlias(true);   //抗锯齿
        paint.setColor(Color.BLACK);
        canvas.drawCircle(200, 100, 100, paint);

        paint.setStrokeWidth(2);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(500, 100, 100, paint);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLUE);
        canvas.drawCircle(200, 300, 100, paint);

        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(15);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(500, 320, 100, paint);

    }

}
