package com.zeros.myself.practice_delete.practice2.practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class Practice01LinearGradientView extends View {

    public Practice01LinearGradientView(Context context) {
        super(context);
    }

    public Practice01LinearGradientView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Practice01LinearGradientView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);


    {
        // 用 Paint.setShader(shader) 设置一个 LinearGradient
        // LinearGradient 的参数：坐标：(100, 100) 到 (500, 500) ；颜色：#E91E63 到 #2196F3
        //TileMode 一共有 3 个值可 选： CLAMP, MIRROR 和 REPEAT。
        Shader shader = new LinearGradient(100, 100, 500, 200, Color.parseColor("#42ae4e"),
                Color.rgb(222, 111, 184), Shader.TileMode.CLAMP);
        paint.setShader(shader);

        //注意：在设置了 Shader 的情况下， Paint.setColor/ARGB() 所设置的颜色 就不再起作用
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawCircle(300, 300, 200, paint);
    }
}
