package com.zeros.myself.practice_delete.practice6.sample;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.zeros.myself.R;

/**
 * @author zxx on 2020/4/17
 */
public class Sample04Alpha extends RelativeLayout {

    public Sample04Alpha(Context context) {
        super(context);
    }

    public Sample04Alpha(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Sample04Alpha(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    Button animaBt;
    ImageView imageView;
    int state = 0;

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        animaBt = findViewById(R.id.animateBt);
        imageView = findViewById(R.id.imageView);

        animaBt.setOnClickListener(v->{
            switch (state) {
                case 0:
                    imageView.animate().alpha(0);
                    break;
                case 1:
                    imageView.animate().alpha(1);
                    break;
            }
            state++;
            if (state == 2) {
                state = 0;
            }
        });
    }
}
