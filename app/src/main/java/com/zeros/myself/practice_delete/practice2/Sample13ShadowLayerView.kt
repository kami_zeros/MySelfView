package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/27.
 */
class Sample13ShadowLayerView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    internal var paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        //在之后的绘制内容下面加一层阴影。
        paint.setShadowLayer(10f, 5f, 5f, Color.RED)
        // radius 是阴影的模糊范围； dx dy 是阴影 的偏移量； shadowColor 是阴影的颜色。
        //如果要清除阴影层，使用 clearShadowLayer() 。

    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        paint.textSize = 120f
        canvas.drawText("Hello 仍无线", 50f, 200f, paint)

    }

}