package com.zeros.myself.practice_delete.practice3

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/30.
 */
class Sample11GetFontSpacingView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    internal var paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        paint.textSize = 60f
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        //获取推荐的行距。
        val spacing: Float = paint.fontSpacing

        canvas.drawText(Constant.text, 50f, 100f, paint)

        canvas.drawText(Constant.text, 50f, 100 + spacing, paint)

        canvas.drawText(Constant.text, 50f, 100 + spacing * 2, paint)
    }

}