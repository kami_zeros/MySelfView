package com.zeros.myself.practice_delete.practice3

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/30.
 */
class Sample03SetTextSizeView : View {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val text = "Hello HenCoder"

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        var y: Float = 100f
        paint.textSize = 16f
        canvas.drawText(text, 50f, y.toFloat(), paint)

        y += 30
        paint.textSize = 24f
        canvas.drawText(text, 50f, y, paint)

        y += 55
        paint.textSize = 48f
        canvas.drawText(text, 50f, y, paint)

        y += 80
        paint.textSize = 72f
        canvas.drawText(text, 50f, y, paint)
    }

}