package com.zeros.myself.practice_delete.practice4.sample

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.zeros.myself.R
import java.util.jar.Attributes

/**
 * @author zqq on 2020-04-13.
 */
class Sample04ScaleView : View {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    var paint = Paint(Paint.ANTI_ALIAS_FLAG)
    var bitmap: Bitmap
    var point1 = Point(200, 200)
    var point2 = Point(600, 200)


    init {
        bitmap = BitmapFactory.decodeResource(resources, R.mipmap.maps)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        var bitmapWidth = bitmap.width
        var bitmapHeight = bitmap.height

        canvas.save()
        canvas.scale(1.3f, 1.3f, (point1.x + bitmapWidth / 2).toFloat(),
                (point1.y + bitmapHeight / 2).toFloat())
        canvas.drawBitmap(bitmap, point1.x.toFloat(), point1.y.toFloat(), paint)
        canvas.restore()

        canvas.save()
        canvas.scale(0.6f, 1.6f, (point2.x + bitmapWidth / 2).toFloat(),
                (point2.y + bitmapHeight / 2).toFloat())

        canvas.drawBitmap(bitmap, point2.x.toFloat(), point2.y.toFloat(), paint)
        canvas.restore()

    }


}