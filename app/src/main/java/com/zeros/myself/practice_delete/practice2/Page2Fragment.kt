package com.zeros.myself.practice_delete.practice2

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import com.zeros.myself.R

/**
 * @author zqq on 2019/12/25.
 */
class Page2Fragment : Fragment() {

    @LayoutRes
    internal var sampleLayoutRes: Int = 0
    @LayoutRes
    internal var practiceLayoutRes: Int = 0

    companion object {
        private const val PRACTICE_RES: String = "practiceLayoutRes"

        fun newInstance(@LayoutRes sampleLayoutRes: Int, @LayoutRes practiceLayoutRes: Int): Page2Fragment {
            val fragment = Page2Fragment()
            val args = Bundle()
            args.putInt("sampleLayoutRes", sampleLayoutRes)
            args.putInt(PRACTICE_RES, practiceLayoutRes)
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater!!.inflate(R.layout.fragment_page2, container, false)

        var sampleStub = view.findViewById<ViewStub>(R.id.sampleStub2)
        sampleStub.layoutResource = sampleLayoutRes
        sampleStub.inflate()

        var practiceStub = view.findViewById<ViewStub>(R.id.practiceStub2)
        practiceStub.layoutResource = practiceLayoutRes
        practiceStub.inflate()

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        if (args != null) {
            sampleLayoutRes = args.getInt("sampleLayoutRes")
            practiceLayoutRes = args.getInt(PRACTICE_RES)
        }
    }


}