package com.zeros.myself.practice_delete.practice3

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/30.
 */
class Sample09SetTextScaleXView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    internal var paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        paint.textSize = 50f

        //设置文字横向放缩。也就是文字变胖变瘦。
        paint.textScaleX = 1.2f
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas!!.drawText(Constant.text, 50f, 100f, paint)
    }

}