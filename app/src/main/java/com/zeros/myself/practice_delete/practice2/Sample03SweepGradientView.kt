package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.SweepGradient
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/27.
 */
class Sample03SweepGradientView : View {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        //「扫描渐变」
        paint.setShader(SweepGradient(300f, 300f, Color.parseColor("#E91E63"),
                Color.parseColor("#2196F3")))

        //cx cy ：扫描的中心
        //color0：扫描的起始颜色
        //color1：扫描的终止颜色
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.drawCircle(300f, 300f, 200f, paint)
    }


}