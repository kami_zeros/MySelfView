package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/25.
 */
class Sample01LinearGradientView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {


    //抗锯齿
    var paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
//        注意：Kotlin中int不会自动转换为float，所以需要添加f 否则报错。
        //着色器--设置渐变
        paint.shader = LinearGradient(100f, 100f, 500f, 500f,
                Color.parseColor("#E91E63"),
                Color.parseColor("#2196F3"), Shader.TileMode.CLAMP)
        //x0 y0 x1 y1：渐变的两个端点的位置
        //color0 color1 是端点的颜色
        // tile：端点范围之外的着色规则，类型是 TileMode。
    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawCircle(300f, 300f, 200f, paint)
    }

}