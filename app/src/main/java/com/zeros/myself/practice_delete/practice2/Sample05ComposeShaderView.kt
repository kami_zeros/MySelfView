package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.zeros.myself.R

/**
 * @author zqq on 2019/12/27.
 */
class Sample05ComposeShaderView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        //混合着色器:就是把两个 Shader 一起使用。

        //使用离屏缓存 把内容 绘制在额外的层上（否则会 出现奇怪的结果了），再把绘制好的内容贴回 View 中。
        //1.Canvas.saveLayer()
        //      saveLayer() 可以做短时的离屏缓冲。使用方法很简单，在绘制代码的前后各 加一行代码，在绘制之前保存，绘制之后恢复：
        //      int saved = canvas.saveLayer(null, null, Canvas.ALL_SAVE_FLAG);
        //      canvas.restoreToCount(saved);
        //2.View.setLayerType() 是直接把整个 View 都绘制在离屏缓冲中。
        //      setLayerType(LAYER_TYPE_HARDWARE) 是使用 GPU 来缓冲，
        //      setLayerType(LAYER_TYPE_SOFTWARE) 是直接直接用一个 Bitmap 来缓冲。
        //如果没有特殊需求，可以选用第一种方法 Canvas.saveLayer() 来设置离屏缓冲， 以此来获得更高的性能。
        setLayerType(LAYER_TYPE_SOFTWARE, null)

        val bitmap1 = BitmapFactory.decodeResource(resources, R.mipmap.batman)
        val shader1 = BitmapShader(bitmap1, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)

        val bitmap2 = BitmapFactory.decodeResource(resources, R.mipmap.batman_logo)
        val shader2 = BitmapShader(bitmap2, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)

        paint.shader=ComposeShader(shader1, shader2, PorterDuff.Mode.DST_IN)

        //mode: 两个 Shader 的叠加模式，即 shaderA 和 shaderB 应该怎样共同绘制。它 的类型是 PorterDuff.Mode 。
        //具体来说， PorterDuff.Mode 一共有 17 个，可以分为两类：
        //Alpha 合成 (Alpha Compositing)
        //混合 (Blending)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.drawCircle(200f, 200f, 200f, paint)
    }

}