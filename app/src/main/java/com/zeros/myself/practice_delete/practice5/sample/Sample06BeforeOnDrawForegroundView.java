package com.zeros.myself.practice_delete.practice5.sample;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * @author zxx on 2020/4/15
 */
public class Sample06BeforeOnDrawForegroundView extends AppCompatImageView {

    public Sample06BeforeOnDrawForegroundView(Context context) {
        super(context);
    }

    public Sample06BeforeOnDrawForegroundView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Sample06BeforeOnDrawForegroundView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    {
        paint.setTextSize(60);
    }

    @Override
    public void onDrawForeground(Canvas canvas) {
        paint.setColor(Color.parseColor("#f44336"));
        canvas.drawRect(0, 40, 200, 120, paint);

        paint.setColor(Color.WHITE);
        canvas.drawText("New", 20, 100, paint);

        super.onDrawForeground(canvas);
    }

}
