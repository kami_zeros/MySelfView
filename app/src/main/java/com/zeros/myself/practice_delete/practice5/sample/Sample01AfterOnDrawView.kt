package com.zeros.myself.practice_delete.practice5.sample

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.v4.BuildConfig
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import com.zeros.myself.R

/**
 * @author zxx on 2020/4/15
 */
class Sample01AfterOnDrawView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    var paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        paint.color = Color.parseColor("#FFC107")
        paint.textSize = 28f
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (BuildConfig.DEBUG) {
            var drawable = drawable
            if (drawable != null) {
                canvas.save()
                canvas.concat(imageMatrix)

                var rect = drawable.bounds
                canvas.drawText(resources.getString(R.string.image_size, rect.width(), rect.height()),
                        20f, 40f, paint)
                canvas.restore()
            }

        }
    }

}