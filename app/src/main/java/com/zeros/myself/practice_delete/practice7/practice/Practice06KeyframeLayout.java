package com.zeros.myself.practice_delete.practice7.practice;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.zeros.myself.R;
import com.zeros.myself.practice_delete.practice7.sample.Sample06KeyframeView;

/**
 * @author zxx on 2020/10/13
 */
public class Practice06KeyframeLayout extends RelativeLayout {

    public Practice06KeyframeLayout(Context context) {
        super(context);
    }

    public Practice06KeyframeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Practice06KeyframeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    Sample06KeyframeView view;
    Button animateBt;

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        view = (Sample06KeyframeView) findViewById(R.id.objectAnimatorView);
        animateBt = (Button) findViewById(R.id.animateBt);

        animateBt.setOnClickListener(v -> {
            // 使用 Keyframe.ofFloat() 来为 view 的 progress 属性创建关键帧
            // 初始帧：progress 为 0
            // 时间进行到一般：progress 为 100
            // 结束帧：progress 回落到 80
            // 使用 PropertyValuesHolder.ofKeyframe() 来把关键帧拼接成一个完整的属性动画方案
            // 使用 ObjectAnimator.ofPropertyValuesHolder() 来创建动画
        });
    }
}
