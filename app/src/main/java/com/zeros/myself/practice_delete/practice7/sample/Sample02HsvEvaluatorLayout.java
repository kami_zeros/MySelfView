package com.zeros.myself.practice_delete.practice7.sample;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.zeros.myself.R;
import com.zeros.myself.practice_delete.practice7.HsvEvaluator;

/**
 * @author zxx on 2020/10/12
 */
public class Sample02HsvEvaluatorLayout extends RelativeLayout {

    public Sample02HsvEvaluatorLayout(Context context) {
        super(context);
    }

    public Sample02HsvEvaluatorLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Sample02HsvEvaluatorLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    Sample02HsvEvaluatorView view;
    Button animateBt;


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        view = findViewById(R.id.objectAnimatorView);
        animateBt = findViewById(R.id.animateBt);

        animateBt.setOnClickListener(v -> {
            ObjectAnimator animator = ObjectAnimator.ofInt(view, "color", 0xffff0000, 0xff00ff00);

            animator.setEvaluator(new HsvEvaluator());
            animator.setInterpolator(new LinearInterpolator());
            animator.setDuration(2000);
            animator.start();
        });
    }
}
