package com.zeros.myself.practice_delete.practice6.sample;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.zeros.myself.R;

/**
 * @author zxx on 2020/4/17
 */
public class Sample03Scale extends RelativeLayout {

    public Sample03Scale(Context context) {
        super(context);
    }

    public Sample03Scale(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Sample03Scale(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    Button animaBt;
    ImageView imageView;
    int size = 0;

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        animaBt = findViewById(R.id.animateBt);
        imageView = findViewById(R.id.imageView);

        animaBt.setOnClickListener(v -> {
            switch (size) {
                case 0:
                    imageView.animate().scaleX(1.5f);
                    break;
                case 1:
                    imageView.animate().scaleX(1);
                    break;
                case 2:
                    imageView.animate().scaleY(.5f);
                    break;
                case 3:
                    imageView.animate().scaleY(1);
                    break;
            }
            size++;
            if (size == 4) {
                size = 0;
            }
        });

    }
}
