package com.zeros.myself.practice_delete.practice4.sample

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.zeros.myself.R

/**
 * @author zqq on 2020-04-13.
 */
class Sample08MatrixScaleView : View {
    var paint = Paint(Paint.ANTI_ALIAS_FLAG)
    var bitmap: Bitmap? = null
    var point1 = Point(200, 200)
    var point2 = Point(600, 200)
    var matrixS: Matrix = Matrix()   //因为View有一个相同的matrix即getMatrix()


    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val bitmapWidth = bitmap!!.width
        val bitmapHeight = bitmap!!.height

        canvas.save()
        matrixS.reset()
        matrixS.postScale(1.3f, 1.3f, point1.x + bitmapWidth / 2.toFloat(),
                point1.y + bitmapHeight / 2.toFloat())
        canvas.concat(matrixS)
        canvas.drawBitmap(bitmap, point1.x.toFloat(), point1.y.toFloat(), paint)
        canvas.restore()


        canvas.save()
        matrixS.reset()
        matrixS.postScale(0.6f, 1.6f, point2.x + bitmapWidth / 2.toFloat(),
                point2.y + bitmapHeight / 2.toFloat())
        canvas.concat(matrixS)
        canvas.drawBitmap(bitmap, point2.x.toFloat(), point2.y.toFloat(), paint)
        canvas.restore()
    }

    init {
        bitmap = BitmapFactory.decodeResource(resources, R.mipmap.maps)
    }
}