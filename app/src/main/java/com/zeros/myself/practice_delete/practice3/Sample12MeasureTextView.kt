package com.zeros.myself.practice_delete.practice3

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/30.
 */
class Sample12MeasureTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    internal var paint1 = Paint(Paint.ANTI_ALIAS_FLAG)
    internal var paint2 = Paint(Paint.ANTI_ALIAS_FLAG)
    internal var text1 = "三个月内你胖了"
    internal var text2 = "4.5"
    internal var text3 = "公斤"
    internal var measuredText1: Float = 0.toFloat()
    internal var measuredText2: Float = 0f


    init {
        paint1.textSize = 60f
        paint2.textSize = 120f

        paint2.color = Color.parseColor("#e91e63")

        //测量文字的宽度并返回。
        measuredText1 = paint1.measureText(text1)
        measuredText2 = paint2.measureText(text2)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawText(text1, 50f, 200f, paint1)
        canvas.drawText(text2, 50 + measuredText1, 200f, paint2)
        canvas.drawText(text3, 50 + measuredText1 + measuredText2, 200f, paint1)
    }

}