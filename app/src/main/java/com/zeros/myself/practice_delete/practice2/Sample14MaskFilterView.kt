package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.zeros.myself.R

/**
 * @author zqq on 2019/12/27.
 */
class Sample14MaskFilterView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val bitmap: Bitmap

    //模糊效果的 MaskFilter。
    //radius 参数是模糊的范围， style 是模糊的类型。一共有四种：
    //NORMAL: 内外都模糊绘制
    //SOLID: 内部正常绘制，外部模糊
    //INNER: 内部模糊，外部不绘制
    //OUTER: 内部不绘制，外部模糊
    val maskFilter1: MaskFilter = BlurMaskFilter(50f, BlurMaskFilter.Blur.NORMAL)//(有可能需要强转)
    val maskFilter2 = BlurMaskFilter(50f, BlurMaskFilter.Blur.INNER)
    val maskFilter3 = BlurMaskFilter(50f, BlurMaskFilter.Blur.OUTER)
    val maskFilter4 = BlurMaskFilter(50f, BlurMaskFilter.Blur.SOLID)


    init {
        //setLayerType(LAYER_TYPE_HARDWARE) 是使用 GPU 来缓冲，
        //setLayerType(LAYER_TYPE_SOFTWARE) 是直接直接用一个 Bitmap 来缓冲。
        setLayerType(LAYER_TYPE_SOFTWARE, null)
        bitmap = BitmapFactory.decodeResource(resources, R.mipmap.what_the_fuck)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        paint.maskFilter = maskFilter1
        canvas.drawBitmap(bitmap, 100f, 50f, paint)

        paint.setMaskFilter(maskFilter2)
        canvas.drawBitmap(bitmap, (bitmap.width + 200).toFloat(), 50f, paint)

        paint.maskFilter = maskFilter3
        canvas.drawBitmap(bitmap, 100f, (bitmap.height + 100).toFloat(), paint)

        paint.maskFilter = maskFilter4
        canvas.drawBitmap(bitmap, (bitmap.width + 200).toFloat(), (bitmap.height + 100).toFloat(), paint)

    }

}