package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/27.
 */
class Sample12PathEffectView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    //使用 PathEffect 来给图形的轮廓设置效果。对 Canvas 所有的图形绘制有效，也 就是 drawLine() drawCircle() drawPath() 这些方法。
    //单一 效果的
    //CornerPathEffect      :把所有拐角变成圆角。
    //DiscretePathEffect    :把线条进行随机的偏离
    //DashPathEffect        :使用虚线来绘制线条。
    //PathDashPathEffect    :使用一个 Path(相当于一个图形) 来绘制「虚线」。

    //和组合效果的
    //SumPathEffect         :就是分别按照两种 PathEffect 分别对目标进行绘制。
    //ComposePathEffect     :先对目标 Path 使用一个 PathEffect，然后再对这个改变后的 Path 使用另一个 PathEffect。

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val path = Path()
    val cornerPathEffect = CornerPathEffect(20f)
    val discretePathEffect = DiscretePathEffect(20f, 5f)
    val dashPathEffect = DashPathEffect(floatArrayOf(20f, 10f, 5f, 10f), 0f)
    val pathDashPathEffect:PathDashPathEffect
    val sumPathEffect = SumPathEffect(dashPathEffect, discretePathEffect)
    val composePathEffect = ComposePathEffect(dashPathEffect, discretePathEffect)

    init {
        paint.style = Paint.Style.STROKE
        path.moveTo(50f, 100f)
        //rLineTo:相对当前 位置
        path.rLineTo(50f, 100f)
        path.rLineTo(80f, -150f)
        path.rLineTo(100f, 100f)
        path.rLineTo(70f, -120f)
        path.rLineTo(150f, 80f)

        val dashPath = Path()
        //lineTo 绝对坐标
        dashPath.lineTo(20f, -30f)
        dashPath.lineTo(40f, 0f)
        dashPath.close()

        //shape 参数是用来绘制的 Path ；
        //advance 是两个相邻的 shape 段之间的 间隔，不过注意，这个间隔是两个 shape 段的起点的间隔，而不是前一个的终点和 后一个的起点的距离；
        //phase 和 DashPathEffect 中一样，是虚线的偏移；
        //最后 一个参数 style，是用来指定拐弯改变的时候 shape 的转换方式。style 的类型 为 PathDashPathEffect.Style，具体有三个值：
        //TRANSLATE：位移
        //ROTATE：旋转
        //MORPH：变体
        pathDashPathEffect = PathDashPathEffect(dashPath, 50f, 0f, PathDashPathEffect.Style.MORPH)

    }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        // 使用 Paint.setPathEffect() 来设置不同的 PathEffect
        // CornerPathEffect
        paint.pathEffect = cornerPathEffect
        canvas.drawPath(path, paint)

        canvas.save()
        canvas.translate(500f, 0f)
        // DiscretePathEffect
        paint.pathEffect = discretePathEffect
        canvas.drawPath(path, paint)
        canvas.restore()

        canvas.save()
        canvas.translate(0f, 200f)
        // DashPathEffect
        paint.pathEffect = dashPathEffect
        canvas.drawPath(path, paint)
        canvas.restore()

        canvas.save()
        canvas.translate(500f, 200f)
        // PathDashPathEffect
        paint.pathEffect = pathDashPathEffect
        canvas.drawPath(path, paint)
        canvas.restore()

        canvas.save()
        canvas.translate(0f, 400f)
        // SumPathEffect
        paint.pathEffect = sumPathEffect
        canvas.drawPath(path, paint)
        canvas.restore()

        canvas.save()
        canvas.translate(500f, 400f)
        // ComposePathEffect
        paint.pathEffect = composePathEffect
        canvas.drawPath(path, paint)
        canvas.restore()

        paint.setPathEffect(null)

    }




}