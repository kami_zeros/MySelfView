package com.zeros.myself.practice_delete.practice3

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/30.
 */
class Sample02StaticLayoutView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val textPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)
    val text = "Hello\nHenCoder"
    var staticLayout: StaticLayout

    init {
        textPaint.textSize = 60f
        //Canvas.drawText() 只能绘制单行的文字，而不能换行。
        // StaticLayout 支持换行，它既可以为文字设 置宽度上限来让文字自动换行，也会在 \n 处主动换行。
        staticLayout = StaticLayout(text, textPaint, 600, Layout.Alignment.ALIGN_NORMAL,
                1f, 0f, true)

    }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.save()
        canvas.translate(50f, 40f)
        staticLayout.draw(canvas)
        canvas.restore()
    }

}