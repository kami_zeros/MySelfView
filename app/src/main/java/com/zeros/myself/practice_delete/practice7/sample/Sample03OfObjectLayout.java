package com.zeros.myself.practice_delete.practice7.sample;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.zeros.myself.R;
import com.zeros.myself.practice_delete.practice7.PointFEvaluator;

/**
 * @author zxx on 2020/10/12
 */
public class Sample03OfObjectLayout extends RelativeLayout {

    public Sample03OfObjectLayout(Context context) {
        super(context);
    }

    public Sample03OfObjectLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Sample03OfObjectLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    Sample03OfObjectView view;
    Button animateBt;


    //View 的 onAttachedToWindow() 的调用时机会发生在 onMeasure() 之前
    //我们在自定义 View 的时候，某些比较重量级的资源，而且不能与其他 View 通用的时候，
    // 就可以重写这两个方法，并在 onAttachedToWindow() 中进行初始化，onDetachedFromWindow() 方法里释放掉。
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        view = (Sample03OfObjectView) findViewById(R.id.objectAnimatorView);
        animateBt = (Button) findViewById(R.id.animateBt);

        animateBt.setOnClickListener(v -> {
            ObjectAnimator animator = ObjectAnimator.ofObject(view, "position",
                    new PointFEvaluator(), new PointF(0, 0), new PointF(1, 1));

            animator.setInterpolator(new LinearInterpolator());
            animator.setDuration(1000);
            animator.start();
        });
    }
}
