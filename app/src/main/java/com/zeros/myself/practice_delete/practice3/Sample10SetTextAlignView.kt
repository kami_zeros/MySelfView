package com.zeros.myself.practice_delete.practice3

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/30.
 */
class Sample10SetTextAlignView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    var paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        paint.textSize = 60f
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        //设置文字的对齐方式。
        paint.textAlign = Paint.Align.LEFT
        canvas.drawText(Constant.text, width / 2f, 100f, paint)

        paint.textAlign = Paint.Align.CENTER
        canvas.drawText(Constant.text, width / 2f, 200f, paint)

        paint.textAlign = Paint.Align.RIGHT
        canvas.drawText(Constant.text, width / 2f, 300f, paint)

    }

}