package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/27.
 */
class Sample02RadialGradientView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        //辐射渐变很好理解，就是从中心向周围辐射状的渐变
        paint.shader = RadialGradient(300f, 300f, 200f, Color.parseColor("#E91E63"),
                Color.parseColor("#2196F3"), Shader.TileMode.CLAMP)

//          centerX centerY：辐射中心的坐标
//          radius：辐射半径
//          centerColor：辐射中心的颜色
//          edgeColor：辐射边缘的颜色
//          tileMode：辐射范围之外的着色模式。
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawCircle(300f, 300f, 200f, paint)
    }

}