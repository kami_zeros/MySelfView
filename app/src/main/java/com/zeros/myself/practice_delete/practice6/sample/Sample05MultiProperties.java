package com.zeros.myself.practice_delete.practice6.sample;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;

import com.zeros.myself.R;

import static com.zeros.myself.practice_delete.practice6.DenstityUtil.dpToPixel;

/**
 * @author zxx on 2020/4/17
 */
public class Sample05MultiProperties extends ConstraintLayout {

    public Sample05MultiProperties(Context context) {
        super(context);
    }

    public Sample05MultiProperties(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Sample05MultiProperties(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    Button animateBt;
    ImageView imageView;
    boolean animated;

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        animateBt = (Button) findViewById(R.id.animateBt);
        imageView = (ImageView) findViewById(R.id.imageView);

        imageView.setScaleX(0);
        imageView.setScaleY(0);
        imageView.setAlpha(0f);

        animateBt.setOnClickListener(v -> {
            if (!animated) {
                imageView.animate()
                        .translationX(dpToPixel(200))
                        .rotation(360)
                        .scaleX(1)
                        .scaleY(1)
                        .alpha(1);
            } else {
                imageView.animate()
                        .translationX(0)
                        .rotation(0)
                        .scaleX(0)
                        .scaleY(0)
                        .alpha(0);
            }
            animated = !animated;
        });

    }
}
