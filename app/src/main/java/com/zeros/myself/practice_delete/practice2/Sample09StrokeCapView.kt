package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

/**
 * 设置线条形状的一共有 4 个方法：
 *      setStrokeWidth(float width),
 *      setStrokeCap(Paint.Cap cap),
 *      setStrokeJoin(Paint.Join join),
 *      setStrokeMiter(float miter) 。
 * @author zqq on 2019/12/27.
 */
class Sample09StrokeCapView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        paint.strokeWidth = 40f
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        //设置线头的形状。线头形状有三种：BUTT 平头、ROUND 圆头、SQUARE 方头。默 认为 BUTT。
        paint.strokeCap = Paint.Cap.BUTT
        canvas.drawLine(50f, 50f, 400f, 50f, paint)

        paint.strokeCap = Paint.Cap.ROUND
        canvas.drawLine(50f, 150f, 400f, 150f, paint)

        paint.strokeCap = Paint.Cap.SQUARE
        canvas.drawLine(50f, 250f, 400f, 250f, paint)

    }

}