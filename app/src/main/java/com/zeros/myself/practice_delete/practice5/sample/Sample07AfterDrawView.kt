package com.zeros.myself.practice_delete.practice5.sample

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet

/**
 * @author zxx on 2020/4/15
 */
class Sample07AfterDrawView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    var paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        paint.setTextSize(60f);
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        paint.color = Color.parseColor("#f44336")
        canvas.drawRect(0f, 40f, 200f, 120f, paint)

        paint.color = Color.WHITE
        canvas.drawText("New", 20f, 100f, paint)
    }
}


