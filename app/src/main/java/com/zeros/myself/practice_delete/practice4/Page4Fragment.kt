package com.zeros.myself.practice_delete.practice4

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import com.zeros.myself.R

/**
 * @author zqq on 2020/1/7.
 */
class Page4Fragment : Fragment() {

    @LayoutRes
    var sampleLayoutRes: Int = 0
    @LayoutRes
    var practiceLayoutRes: Int = 0

    companion object {
        const val PRACTICE_RES: String = "practiceLayoutRes"

        fun newInstance(sampleLayoutRes: Int, practiceLayoutRes: Int): Page4Fragment {
            val fragment = Page4Fragment()
            val args = Bundle()
            args.putInt("sampleLayoutRes", sampleLayoutRes)
            args.putInt(PRACTICE_RES, practiceLayoutRes)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundles = arguments
        if (bundles != null) {
            sampleLayoutRes = bundles.getInt("sampleLayoutRes")
            practiceLayoutRes = bundles.getInt(PRACTICE_RES)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_page3, container, false)

        val sampleStub: ViewStub = view.findViewById(R.id.sampleStub3)
        sampleStub.layoutResource = sampleLayoutRes
        sampleStub.inflate()

        val practiceStub = view.findViewById<ViewStub>(R.id.practiceStub3)
        practiceStub.layoutResource = practiceLayoutRes
        practiceStub.inflate()

        return view
    }

}