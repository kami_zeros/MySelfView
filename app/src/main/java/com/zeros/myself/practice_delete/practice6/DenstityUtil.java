package com.zeros.myself.practice_delete.practice6;

import android.content.res.Resources;
import android.util.DisplayMetrics;

/**
 * @author zxx on 2020/4/16
 */
public class DenstityUtil {

    public static float dpToPixel(float dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return dp * metrics.density;
    }

}
