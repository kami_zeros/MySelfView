package com.zeros.myself.practice_delete.practice3

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/30.
 */
class Sample05SetFakeBoldTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val text = "Hello HenCoder"

    init {
        paint.textSize = 50f

        //是否使用伪粗体。
        paint.isFakeBoldText = true
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas!!.drawText(text, 50f, 100f, paint)

    }

}