package com.zeros.myself.practice_delete.practice6.sample;

import android.content.Context;
import android.graphics.Outline;
import android.graphics.Path;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.zeros.myself.R;
import com.zeros.myself.practice_delete.practice6.DenstityUtil;

import static android.os.Build.VERSION.SDK_INT;
import static com.zeros.myself.practice_delete.practice6.DenstityUtil.dpToPixel;

/**
 * @author zqq on 2020-04-16.
 */
public class Sample01Translation extends RelativeLayout {

    public Sample01Translation(Context context) {
        super(context);
    }

    public Sample01Translation(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Sample01Translation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    Button animateBt;
    ImageView imageView;

    int translationStateCount = SDK_INT > Build.VERSION_CODES.LOLLIPOP ? 6 : 4;
    int translationState = 0;


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        animateBt = findViewById(R.id.animateBt);
        imageView = findViewById(R.id.imageView);

        if (SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            // 给音乐图标加上合适的阴影
            imageView.setOutlineProvider(new MusicOutlineProvider());
        }

        animateBt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (translationState) {
                    case 0:
                        imageView.animate().translationX(DenstityUtil.dpToPixel(100));
                        break;
                    case 1:
                        imageView.animate().translationX(0);
                        break;
                    case 2:
                        imageView.animate().translationY(DenstityUtil.dpToPixel(50));
                        break;
                    case 3:
                        imageView.animate().translationY(0);
                        break;
                    case 4:
                        if (SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            imageView.animate().translationZ(dpToPixel(15));
                        }
                        break;
                    case 5:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            imageView.animate().translationZ(0);
                        }
                        break;
                }
                translationState++;
                if (translationState == translationStateCount) {
                    translationState = 0;
                }
            }
        });
    }
}

/**
 * 为音乐图标设置三角形的 Outline。
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
class MusicOutlineProvider extends ViewOutlineProvider {

    Path path = new Path();

    {
        path.moveTo(0, dpToPixel(10));
        path.lineTo(dpToPixel(7), dpToPixel(2));
        path.lineTo(dpToPixel(116), dpToPixel(58));
        path.lineTo(dpToPixel(116), dpToPixel(70));
        path.lineTo(dpToPixel(7), dpToPixel(128));
        path.lineTo(0, dpToPixel(120));
        path.close();
    }

    @Override
    public void getOutline(View view, Outline outline) {
        outline.setConvexPath(path);
    }
}
