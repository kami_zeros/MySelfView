package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.zeros.myself.R

/**
 * @author zqq on 2019/12/27.
 */
class Sample08XfermodeView : View {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    var bitmap1: Bitmap = BitmapFactory.decodeResource(resources, R.mipmap.batman)
    var bitmap2: Bitmap

    //"Xfermode" 其实就是 "Transfer mode"，用 "X" 来代替 "Trans"
    //创建 Xfermode 的时候其实是创建的它的子类 PorterDuffXfermode。
    // PorterDuff.Mode 一共有 17 个，可以分为两类：
    //1.Alpha 合成 (Alpha Compositing)
    //2.混合 (Blending)
    var xfermode1: Xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC)
    var xfermode2: Xfermode
    var xfermode3: Xfermode? = null


    init {
        bitmap2 = BitmapFactory.decodeResource(resources, R.mipmap.batman_logo)
        xfermode2 = PorterDuffXfermode(PorterDuff.Mode.DST_IN)
        xfermode3 = PorterDuffXfermode(PorterDuff.Mode.DST_OUT)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        //使用 setXfermode() 正常绘制，必须使用离屏缓存 (Off-screen Buffer) 把内容 绘制在额外的层上（否则会 出现奇怪的结果了），再把绘制好的内容贴回 View 中。
        //1.Canvas.saveLayer()
        //  saveLayer() 可以做短时的离屏缓冲。使用方法很简单，在绘制代码的前后各 加一行代码，在绘制之前保存，绘制之后恢复：canvas.restoreToCount(saved);

        //2.View.setLayerType()
        //  View.setLayerType() 是直接把整个 View 都绘制在离屏缓冲中。
        //      setLayerType(LAYER_TYPE_HARDWARE) 是使用 GPU 来缓冲，
        //      setLayerType(LAYER_TYPE_SOFTWARE) 是直接直接用一个 Bitmap 来缓冲。
        //如果没有特殊需求，可以选用第一种方法 Canvas.saveLayer() 来设置离屏缓冲， 以此来获得更高的性能。

        var saved: Int = canvas!!.saveLayer(null, null, Canvas.ALL_SAVE_FLAG)

        canvas.drawBitmap(bitmap1, 0f, 0f, paint)

        paint.setXfermode(xfermode1)
        canvas.drawBitmap(bitmap2, 0f, 0f, paint)
        //用完及时清除 Xfermode
        paint.setXfermode(null)

        canvas.drawBitmap(bitmap1, bitmap1.width + 100f, 0f, paint)
        paint.setXfermode(xfermode2)
        canvas.drawBitmap(bitmap2, bitmap1.width + 100f, 0f, paint)
        paint.setXfermode(null)

        canvas.drawBitmap(bitmap1, 0f, bitmap1.height + 20f, paint)
        paint.setXfermode(xfermode3)
        canvas.drawBitmap(bitmap2, 0f, bitmap1.height + 20f, paint)
        paint.setXfermode(null)


        canvas.restoreToCount(saved)
    }

}