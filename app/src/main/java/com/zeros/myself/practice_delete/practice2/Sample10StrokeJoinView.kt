package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/27.
 */
class Sample10StrokeJoinView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val path = Path()

    init {
        //设置拐角的形状。有三个值可以选择：MITER 尖角、 BEVEL 平角和 ROUND 圆角。
        paint.strokeWidth = 40f
        paint.style = Paint.Style.STROKE

        path.rLineTo(200f, 0f)
        path.rLineTo(-160f, 120f)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.save()
        canvas.translate(50f, 100f)
        paint.strokeJoin = Paint.Join.MITER
        canvas.drawPath(path, paint)

        canvas.translate(300f, 0f)
        paint.strokeJoin = Paint.Join.BEVEL
        canvas.drawPath(path, paint)

        canvas.translate(300f, 0f)
        paint.strokeJoin = Paint.Join.ROUND
        canvas.drawPath(path, paint)

        canvas.restore()
    }

}