package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.zeros.myself.R

/**
 * @author zqq on 2019/12/27.
 */
class Sample07ColorMatrixColorFilterView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    var bitmap: Bitmap? = null

    //过滤颜色
    init {
        bitmap = BitmapFactory.decodeResource(resources, R.mipmap.batman)

        //ColorMatrixColorFilter 使用一个 ColorMatrix 来对颜色进行 处理。 ColorMatrix 这个类，内部是一个 4x5 的矩阵
        val colorMatrix = ColorMatrix()
        //设置饱和度
        colorMatrix.setSaturation(0f)
        val colorFilter = ColorMatrixColorFilter(colorMatrix)
        paint.colorFilter = colorFilter
    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.drawBitmap(bitmap!!, 0f, 0f, paint)

    }

}
