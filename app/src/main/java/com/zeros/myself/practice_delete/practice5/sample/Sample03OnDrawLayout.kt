package com.zeros.myself.practice_delete.practice5.sample

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.widget.LinearLayout

/**
 * @author zxx on 2020/4/15
 */
class Sample03OnDrawLayout : LinearLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    var paint = Paint(Paint.ANTI_ALIAS_FLAG)
    var pattern = Pattern()

    init {
        //ViewGroup 需要主动开启 dispatchDraw() 以外的绘制
        setWillNotDraw(false)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        pattern.draw(canvas!!)
    }

    inner class Pattern {
        private val PATTERN_RATIO = 5f / 6
        var patternPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        lateinit var spots: Array<Spot>

        constructor() {
            spots[0] = Spot(0.24f, 0.3f, 0.026f)
            spots[1] = Spot(0.69f, 0.25f, 0.067f)
            spots[2] = Spot(0.32f, 0.6f, 0.067f)
            spots[3] = Spot(0.62f, 0.78f, 0.083f)
        }

        constructor(spots: Array<Spot>) {
            this.spots = spots
        }

        init {
            patternPaint.color = Color.parseColor("#A0E91E63")
        }

        fun draw(canvas: Canvas) {
            var repition = Math.ceil((width / height).toDouble()).toInt()

            for (i in 0 until spots.size * repition) {
                var spot: Spot = spots[i % spots.size]
                canvas.drawCircle(i / spots.size * height * PATTERN_RATIO + spot.relativeX * height,
                        spot.relativeY * height, spot.relativeSize * height,
                        patternPaint)
            }
        }

        inner class Spot(relativeX: Float, relativeY: Float, relativeSize: Float) {
            var relativeX = 0f
            var relativeY = 0f
            var relativeSize = 0f

            init {
                this.relativeX = relativeX
                this.relativeY = relativeY
                this.relativeSize = relativeSize
            }
        }


    }

}