package com.zeros.myself.practice_delete.practice4.sample

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.zeros.myself.R

/**
 * @author zqq on 2020-04-13.
 */
class Sample02ClipPathView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    var paint = Paint(Paint.ANTI_ALIAS_FLAG)
    var bitmap: Bitmap
    var path1 = Path()
    var path2 = Path()
    var point1 = Point(200, 200)
    var point2 = Point(200, 200)

    init {
        bitmap = BitmapFactory.decodeResource(resources, R.mipmap.maps)
        path1.addCircle(point1.x + 200f, point1.y + 200f, 150f, Path.Direction.CW)

        path2.fillType = Path.FillType.INVERSE_WINDING
        path2.addCircle(point2.x + 200f, point2.y + 200f, 150f, Path.Direction.CW)
    }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.save()
        canvas.clipPath(path1)
        canvas.drawBitmap(bitmap, point1.x.toFloat(), point1.y.toFloat(), paint)
        canvas.restore()

        canvas.save()
        canvas.clipPath(path2)
        canvas.drawBitmap(bitmap, point2.x.toFloat(), point2.y.toFloat(), paint)
        canvas.restore()

    }


}