package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.zeros.myself.R

/**
 * @author zqq on 2019/12/27.
 */
class Sample04BitmapShaderView : View {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)


    init {
        //其实也就是用 Bitmap 的像素来作为图形 或文字的填充。
        val bitmap = BitmapFactory.decodeResource(resources, R.mipmap.batman)
        paint.setShader(BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP))

        //bitmap：用来做模板的 Bitmap 对象
        //tileX：横向的 TileMode
        //tileY：纵向的 TileMode。
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.drawCircle(200f, 200f, 200f, paint)

    }

}