package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.zeros.myself.R

/**
 * 为绘制设置颜色过滤。在 Paint 里设置 ColorFilter ，使用的是 Paint.setColorFilter(ColorFilter filter) 方法。
 * ColorFilter 并不直接 使用，而是使用它的子类。它共有三个子类：
 *      LightingColorFilter
 *      PorterDuffColorFilter 和
 *      ColorMatrixColorFilter。
 * @author zqq on 2019/12/27.
 */
class Sample06LightingColorFilterView : View {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    lateinit var bitmap: Bitmap
    private val colorFilter1 = LightingColorFilter(0x00ffff, 0x000000)
    private val colorFilter2 = LightingColorFilter(0xffffff, 0x003000)
    //参数里的 mul 和 add 都是和颜色 值格式相同的 int 值，其中 mul 用来和目标像素相乘，add 用来和目标像素相加：

    init {
        //模拟简单的光照效果的。
        bitmap = BitmapFactory.decodeResource(resources, R.mipmap.batman)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        paint.colorFilter = colorFilter1
        canvas?.drawBitmap(bitmap, 0f, 0f, paint)

        paint.setColorFilter(colorFilter2)
        canvas?.drawBitmap(bitmap, bitmap.width + 100f, 0f, paint)

    }

}