package com.zeros.myself.practice_delete.practice3

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/30.
 */
class Sample04SetTypefaceView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    val text = "Hello HenCoder"
    var typeface: Typeface      //设置字体。

    init {
        paint.textSize = 50f
        //设置字体。
        typeface = Typeface.createFromAsset(getContext().assets, "Satisfy-Regular.ttf")
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        paint.setTypeface(null)
        canvas.drawText(text, 50f, 100f, paint)

        paint.typeface = Typeface.SERIF
        canvas.drawText(text, 50f, 200f, paint)

        paint.typeface = typeface
        canvas.drawText(text, 50f, 300f, paint)
    }

}