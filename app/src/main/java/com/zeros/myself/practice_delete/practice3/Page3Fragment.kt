package com.zeros.myself.practice_delete.practice3

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import com.zeros.myself.R

/**
 * @author zqq on 2019/12/30.
 */
class Page3Fragment : Fragment() {

    @LayoutRes
    internal var sampleLayoutRes: Int = 0
    @LayoutRes
    internal var practiceLayoutRes: Int = 0

    companion object {
        private const val PRACTICE_RES: String = "practiceLayoutRes"

        fun newInstance(@LayoutRes sampleLayoutRes: Int, @LayoutRes practiceLayoutRes: Int): Page3Fragment {
            val fragment = Page3Fragment()
            val args = Bundle()
            args.putInt("sampleLayoutRes", sampleLayoutRes)
            args.putInt(PRACTICE_RES, practiceLayoutRes)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_page3, container, false)

        val sampleStub = view.findViewById<ViewStub>(R.id.sampleStub3)
        sampleStub.layoutResource = sampleLayoutRes
        sampleStub.inflate()

        val practice = view.findViewById<ViewStub>(R.id.practiceStub3)
        practice.layoutResource = practiceLayoutRes
        practice.inflate()

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        if (args != null) {
            sampleLayoutRes = args.getInt("sampleLayoutRes")
            practiceLayoutRes = args.getInt(PRACTICE_RES)
        }
    }

}