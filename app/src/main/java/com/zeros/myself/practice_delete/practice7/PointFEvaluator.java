package com.zeros.myself.practice_delete.practice7;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.graphics.PointF;
import android.view.View;

/**
 * @author zxx on 2020/4/16
 */
public class PointFEvaluator implements TypeEvaluator<PointF> {

    PointF newPoint = new PointF();

    @Override
    public PointF evaluate(float fraction, PointF startValue, PointF endValue) {
        float x = startValue.x + (fraction * (endValue.x - startValue.x));
        float y = startValue.y + (fraction * (endValue.y - startValue.y));

        newPoint.set(x, y);

        return newPoint;
    }


    //用法
    public void uesd(View view) {
        ObjectAnimator animator = ObjectAnimator.ofObject(view, "position",
                new PointFEvaluator(), new PointF(0, 0), new PointF(1, 1));
        animator.start();
    }
}
