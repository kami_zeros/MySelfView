package com.zeros.myself.practice_delete.practice2

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View

/**
 * @author zqq on 2019/12/27.
 */
class Sample15FillPathView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    internal var paint = Paint(Paint.ANTI_ALIAS_FLAG)
    internal var pathPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    internal var path = Path()
    internal var path1 = Path()
    internal var path2 = Path()
    internal var path3 = Path()

    init {
        //通过 getFillPath(src, dst) 方法就能获取这个实际 Path。方法的参数 里，src 是原 Path ，而 dst 就是实际 Path 的保存位置。
        path.moveTo(50f, 100f)
        path.rLineTo(50f, 100f)
        path.rLineTo(80f, -150f)
        path.rLineTo(100f, 100f)
        path.rLineTo(70f, -120f)
        path.rLineTo(150f, 80f)

        pathPaint.style = Paint.Style.STROKE
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        // 使用 Paint.getFillPath() 获取实际绘制的 Path
        // 第一处：获取 Path
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.strokeWidth = 0f
        paint.getFillPath(path, path1)
        canvas.drawPath(path, paint)

        canvas.save()
        canvas.translate(500f, 0f)
        canvas.drawPath(path1, pathPaint)//保存的path1
        canvas.restore()


        // 第二处：设置 Style 为 STROKE 后再获取 Path
        canvas.save()
        canvas.translate(0f, 200f)
        paint.style = Paint.Style.STROKE
        paint.getFillPath(path, path2)
        canvas.drawPath(path, paint)
        canvas.restore()

        canvas.save()
        canvas.translate(500f, 200f)
        canvas.drawPath(path2, pathPaint)
        canvas.restore()

        // 第三处：Style 为 STROKE 并且线条宽度为 40 时的 Path
        canvas.save()
        canvas.translate(0f, 400f)
        paint.strokeWidth = 40f
        paint.getFillPath(path, path3)
        canvas.drawPath(path, paint)
        canvas.restore()

        canvas.save()
        canvas.translate(500f, 400f)
        canvas.drawPath(path3, pathPaint)
        canvas.restore()
    }

}