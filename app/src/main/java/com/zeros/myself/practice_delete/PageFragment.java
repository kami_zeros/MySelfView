package com.zeros.myself.practice_delete;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import com.zeros.myself.R;

/**
 * wc
 */
public class PageFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @LayoutRes
    private int sampleLayoutRes;
    @LayoutRes
    private int practiceLayoutRes;

    public static PageFragment newInstance(@LayoutRes int sampleLayoutRes, @LayoutRes int practiceLayoutRes) {
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, sampleLayoutRes);
        args.putInt(ARG_PARAM2, practiceLayoutRes);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sampleLayoutRes = getArguments().getInt(ARG_PARAM1);
            practiceLayoutRes = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page, container, false);
        ViewStub sample = view.findViewById(R.id.sampleStub);
        sample.setLayoutResource(sampleLayoutRes);
        sample.inflate();

        ViewStub practice = view.findViewById(R.id.practiceStub);
        practice.setLayoutResource(practiceLayoutRes);
        practice.inflate();
        return view;
    }

}
