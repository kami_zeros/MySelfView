package com.zeros.myself.practice_delete.practice5;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import com.zeros.myself.R;

/**
 * @author zqq on 2020-04-14.
 */
public class Page5Fragment extends Fragment {

    @LayoutRes
    int sampleLayoutRes;
    @LayoutRes
    int practiceLayoutRes;

    public static Page5Fragment newInstance(@LayoutRes int sampleLayoutRes, @LayoutRes int practiceLayoutRes) {
        Page5Fragment fragment = new Page5Fragment();
        Bundle bundle = new Bundle();
        bundle.putInt("sam", sampleLayoutRes);
        bundle.putInt("prac", practiceLayoutRes);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            sampleLayoutRes = bundle.getInt("sam");
            practiceLayoutRes = bundle.getInt("prac");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page3, container, false);

        ViewStub sam = view.findViewById(R.id.sampleStub3);
        sam.setLayoutResource(sampleLayoutRes);
        sam.inflate();

        ViewStub prac = view.findViewById(R.id.practiceStub3);
        prac.setLayoutResource(practiceLayoutRes);
        prac.inflate();

        return view;
    }
}
