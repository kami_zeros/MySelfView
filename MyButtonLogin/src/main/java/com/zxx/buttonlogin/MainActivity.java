package com.zxx.buttonlogin;

import android.animation.ValueAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.zxx.buttonlogin.progressbtn.CircularProgressButton;

public class MainActivity extends AppCompatActivity {

    private CircularProgressButton button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.btn);
        button.setIndeterminateProgressMode(true);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (button.getProgress() == 0) {
                    button.setProgress(50);
                }
            }
        });


        //-------------------1-----------------------
        final CircularProgressButton circularButton1 = findViewById(R.id.circularButton1);
        circularButton1.setIndeterminateProgressMode(true);
        circularButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (circularButton1.getProgress() == 0) {
                    circularButton1.setProgress(50);
                } else if (circularButton1.getProgress() == 100) {
                    circularButton1.setProgress(0);
                } else {
                    circularButton1.setProgress(100);
                }
            }
        });

        final CircularProgressButton circularButton2 = findViewById(R.id.circularButton2);
        circularButton2.setIndeterminateProgressMode(true);
        circularButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (circularButton2.getProgress() == 0) {
                    circularButton2.setProgress(50);
                } else if (circularButton2.getProgress() == -1) {
                    circularButton2.setProgress(0);
                } else {
                    circularButton2.setProgress(-1);
                }
            }
        });

//        ----------2222222-----------
        final CircularProgressButton btn3 = findViewById(R.id.btn3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn3.getProgress() == 0) {
                    simulateSuccessProgress(btn3);
                } else {
                    btn3.setProgress(0);
                }
            }
        });

        final CircularProgressButton btn4 = findViewById(R.id.btn4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn4.getProgress() == 0) {
                    simulateErrorProgress(btn4);
                } else {
                    btn4.setProgress(0);
                }
            }
        });

//        -----3333333333333--
        final CircularProgressButton btn5 = findViewById(R.id.btn5);
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn5.getProgress() == 0) {
                    btn5.setProgress(100);
                } else {
                    btn5.setProgress(0);
                }
            }
        });

        final CircularProgressButton btn6 = findViewById(R.id.btn6);
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn6.getProgress() == 0) {
                    btn6.setProgress(-1);
                } else {
                    btn6.setProgress(0);
                }
            }
        });

//        ---------------4444444444
        final CircularProgressButton btn7 =  findViewById(R.id.btn7);
        btn7.setIndeterminateProgressMode(true);
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn7.getProgress() == 0) {
                    btn7.setProgress(50);
                } else if (btn7.getProgress() == 100) {
                    btn7.setProgress(0);
                } else {
                    btn7.setProgress(100);
                }
            }
        });


        //-----------5555555555
        final CircularProgressButton btn8 = (CircularProgressButton) findViewById(R.id.btn8);
        btn8.setIndeterminateProgressMode(true);
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn8.getProgress() == 0) {
                    btn8.setProgress(50);
                } else if (btn8.getProgress() == 100) {
                    btn8.setProgress(0);
                } else {
                    btn8.setProgress(100);
                }
            }
        });

        final CircularProgressButton btn9 = (CircularProgressButton) findViewById(R.id.btn9);
        btn9.setIndeterminateProgressMode(true);
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn9.getProgress() == 0) {
                    btn9.setProgress(50);
                } else if (btn9.getProgress() == -1) {
                    btn9.setProgress(0);
                } else {
                    btn9.setProgress(-1);
                }
            }
        });


    }

    private void simulateSuccessProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
            }
        });
        widthAnimation.start();
    }

    private void simulateErrorProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 99);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
                if (value == 99) {
                    button.setProgress(-1);
                }
            }
        });
        widthAnimation.start();
    }

}
