package com.zxx.buttonlogin.progressbtn;

/**
 * @author zxx on 2019/8/6
 */
public interface OnAnimationEndListener {

    void onAnimationEnd();

}
