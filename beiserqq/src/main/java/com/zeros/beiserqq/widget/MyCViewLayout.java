package com.zeros.beiserqq.widget;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.zeros.beiserqq.R;
import com.zeros.beiserqq.bean.TheCoordinate;
import com.zeros.beiserqq.utils.DipUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * ▏2018/5/3.
 */

public class MyCViewLayout extends RelativeLayout {

    private Paint mPaint;
    private Path mPath;
    // 贝赛尔曲线成员变量(起始点，控制（操作点），终止点，3点坐标)
    private int startX, startY, controlX, controlY, endX, endY;
    //添加view的个数
    public static int VIEWNUM = 5;
    //控制坐标平均分的比例的
    private double t;
    //坐标点集合
    private List<TheCoordinate> mList;
    private List<ImageView> iList;
    //移动的view
    private ImageView moveView;
    private int tag = 0;
    private Handler handler;
    public static int VIEWWIDTH;//moveview的宽度
    public static int VIEWHEIGHT;//moveview的高度

    public MyCViewLayout(Context context) {
        this(context, null);
    }

    @SuppressLint("HandlerLeak")
    public MyCViewLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE);
        mList = new ArrayList<TheCoordinate>();
        iList = new ArrayList<ImageView>();
        moveView = new ImageView(getContext());
        VIEWWIDTH = dip2px(context, 34);
        VIEWHEIGHT = dip2px(context, 34);
        moveView.setImageDrawable(getResources().getDrawable(R.mipmap.icon_1));
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 1) {
                    firstLoadAniomation();
                }
            }
        };
    }

    public MyCViewLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.e("test", 2 + "");
        mPath = new Path();
        startX = 0;
        startY = getHeight() / 2 + 100;
        mPath.moveTo(startX, startY);
        controlX = getWidth() / 2 + 300;
        controlY = getHeight() / 2 + 100;
        endX = getWidth() + 100;
        endY = 0;
        mPath.quadTo(controlX, controlY, endX, endY);//画贝瑟尔曲线
        canvas.drawPath(mPath, mPaint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        startX = 0;
        startY = getHeight() / 2 + 100;
        controlX = getWidth() / 2 + 300;
        controlY = getHeight() / 2 + 100;
        endX = getWidth() + 100;
        endY = 0;
        addView(4);
        addMoveView(moveView);
        setViewClickEvent();
        this.setBackgroundColor(Color.parseColor("#ffffff"));
        handler.sendEmptyMessageDelayed(1, 1500);
    }

    public void addView(int vNum) {
        Log.e("test", 1 + "");
        for (int i = 0; i < vNum; i++) {//VIEWNUM
            t = ((double) i) / ((double) vNum);//VIEWNUM
            int x = getCurvePoint(startX, controlX, endX);
            int y = getCurvePoint(startY, controlY, endY);
            TheCoordinate coordinate = new TheCoordinate();
            coordinate.setCoordinateX(x);
            coordinate.setCoordinateY(y);
            mList.add(coordinate);
            Log.e("test" + i + "", "t:" + t + "x:" + x + "y:" + y);
        }
        initImageView(mList);
    }

    //    添加imageView
    public void addMoveView(ImageView imageView) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.width = VIEWWIDTH;
        params.height = VIEWHEIGHT;
        params.setMargins(mList.get(0).getCoordinateX(), mList.get(0).getCoordinateY() - params.height / 2, mList.get(0).getCoordinateX() + params.width, mList.get(0).getCoordinateY() + params.height);
        tag = 0;
        imageView.setLayoutParams(params);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
//        removeView(imageView);
        this.addView(imageView);
    }

    private void setViewClickEvent() {
        for (int i = 0; i < iList.size(); i++) {
            iList.get(i).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("test", "tag:" + tag + "" + "vid:" + v.getId() + "");
                    clickAnimation(v.getId());
                    tag = v.getId();
                }
            });
        }
        moveView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                moveViewAnimation();
            }
        });
    }

    private void initImageView(List<TheCoordinate> list) {
        for (int i = 0; i < list.size(); i++) {
            ImageView imageView = new ImageView(getContext());
            RelativeLayout.LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            params.width = VIEWWIDTH;
            params.height = VIEWHEIGHT;
            if (i < 4) {
                if (i == 0) {
                    params.setMargins(list.get(i).getCoordinateX(), list.get(i).getCoordinateY() - params.height / 2, list.get(i).getCoordinateX() + params.width, list.get(i).getCoordinateY() + params.height);
                } else {
                    params.setMargins(list.get(i).getCoordinateX() - params.width / 2, list.get(i).getCoordinateY() - params.height / 2, list.get(i).getCoordinateX() + params.width, list.get(i).getCoordinateY() + params.height);
                }
                imageView.setBackgroundColor(Color.WHITE);
                imageView.setLayoutParams(params);
                imageView.setId(i);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setImageResource(R.mipmap.icon_2017);
                this.addView(imageView);
            }
            iList.add(imageView);
        }
    }


    /**
     * 根据点击的view移动moveview
     */
    private void clickAnimation(int vId) {
        if (tag == 0 && vId == 1) {
            moveAnimation(mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY());
        }
        if (tag == 0 && vId == 2) {
            moveAnimation(0, 0, mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY());
        }
        if (tag == 0 && vId == 3) {
            moveAnimation(0, 0, mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(3).getCoordinateX() - mList.get(2).getCoordinateX() + mList.get(2).getCoordinateX()
                            - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(3).getCoordinateY() - mList.get(2).getCoordinateY() + mList.get(2).getCoordinateY()
                            - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY()
            );
        }
        if (tag == 1 && vId == 2) {
            moveAnimation(mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY());
        }
        if (tag == 2 && vId == 3) {
            moveAnimation(
                    mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(3).getCoordinateX() - mList.get(2).getCoordinateX() + mList.get(2).getCoordinateX()
                            - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(3).getCoordinateY() - mList.get(2).getCoordinateY() + mList.get(2).getCoordinateY()
                            - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY()
            );
        }
        if (tag == 3 && vId == 0) {
            moveAnimation(
                    mList.get(3).getCoordinateX() - mList.get(2).getCoordinateX() + mList.get(2).getCoordinateX()
                            - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(3).getCoordinateY() - mList.get(2).getCoordinateY() + mList.get(2).getCoordinateY()
                            - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    0, 0
            );
        }
        if (tag == 3 && vId == 1) {
            moveAnimation(
                    mList.get(3).getCoordinateX() - mList.get(2).getCoordinateX() + mList.get(2).getCoordinateX()
                            - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(3).getCoordinateY() - mList.get(2).getCoordinateY() + mList.get(2).getCoordinateY()
                            - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY()
            );
        }
        if (tag == 3 && vId == 2) {
            moveAnimation(mList.get(3).getCoordinateX() - mList.get(2).getCoordinateX() + mList.get(2).getCoordinateX()
                            - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(3).getCoordinateY() - mList.get(2).getCoordinateY() + mList.get(2).getCoordinateY()
                            - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY());
        }
        if (tag == 2 && vId == 1) {
            moveAnimation(
                    mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY()
            );
        }
        if (tag == 2 && vId == 0) {
            moveAnimation(
                    mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    0, 0
            );
        }
        if (tag == 1 && vId == 0) {
            moveAnimation(
                    mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    0, 0
            );
        }
        if (tag == 1 && vId == 3) {
            moveAnimation(mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                    mList.get(3).getCoordinateX() - mList.get(2).getCoordinateX() + mList.get(2).getCoordinateX()
                            - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                    mList.get(3).getCoordinateY() - mList.get(2).getCoordinateY() + mList.get(2).getCoordinateY()
                            - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY()
            );
        }
    }

    private void moveAnimation(int startPointX, int startPointY) {
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(moveView, "TranslationX", 0, startPointX);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(moveView, "TranslationY", 0, startPointY);
        AnimatorSet set = new AnimatorSet();
        set.setDuration(800);
        set.setInterpolator(new FastOutSlowInInterpolator());
        set.playTogether(animatorX, animatorY);
        set.start();
    }

    private void moveAnimation(int startPointX, int startPointY, int X1, int Y1, int endPointX, int endPointY) {
        Log.e("test", "startPointX:" + startPointX + "startPointY:" + startPointY + "endPointX:" + endPointX + "endPointY:" + endPointY + "");
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(moveView, "TranslationX", startPointX, X1, endPointX);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(moveView, "TranslationY", startPointY, Y1, endPointY);
        AnimatorSet set = new AnimatorSet();
        set.setDuration(1000);
        set.setInterpolator(new FastOutSlowInInterpolator());
        set.playTogether(animatorX, animatorY);
        set.start();
    }

    private void moveAnimation(int startPointX, int startPointY, int X1, int Y1, int X2, int Y2, int endPointX, int endPointY) {
        Log.e("test", "startPointX:" + startPointX + "startPointY:" + startPointY + "endPointX:" + endPointX + "endPointY:" + endPointY + "");
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(moveView, "TranslationX", startPointX, X1, X2, endPointX);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(moveView, "TranslationY", startPointY, Y1, Y2, endPointY);
        AnimatorSet set = new AnimatorSet();
        set.setDuration(1000);
        set.setInterpolator(new FastOutSlowInInterpolator());
        set.playTogether(animatorX, animatorY);
        set.start();
    }

    private void moveAnimation(int startPointX, int startPointY, int X1, int Y1, int X2, int Y2, int endPointX, int endPointY, int X3, int Y3, int X4, int Y4) {
        Log.e("test", "startPointX:" + startPointX + "startPointY:" + startPointY + "endPointX:" + endPointX + "endPointY:" + endPointY + "");
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(moveView, "TranslationX", startPointX, X1, X2, endPointX, X3, X4, 0);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(moveView, "TranslationY", startPointY, Y1, Y2, endPointY, Y3, Y4, 0);
        AnimatorSet set = new AnimatorSet();
        set.setDuration(3000);
//        set.setInterpolator(new FastOutSlowInInterpolator());
        set.setInterpolator(new LinearInterpolator());
        set.playTogether(animatorX, animatorY);
        set.start();
    }

    private void moveAnimation(int startPointX, int startPointY, int endPointX, int endPointY) {
        Log.e("test", "startPointX:" + startPointX + "startPointY:" + startPointY + "endPointX:" + endPointX + "endPointY:" + endPointY + "");
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(moveView, "TranslationX", startPointX, endPointX);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(moveView, "TranslationY", startPointY, endPointY);
        AnimatorSet set = new AnimatorSet();
        set.setDuration(1000);
        set.setInterpolator(new FastOutSlowInInterpolator());
        set.playTogether(animatorX, animatorY);
        set.start();
    }

    private void moveViewAnimation() {
        ObjectAnimator mAnimator = ObjectAnimator.ofFloat(moveView, "rotation", 0F, 720f);
        mAnimator.setDuration(800);
        mAnimator.start();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
    }





    /**
     * 二阶贝塞尔曲线
     * B(t)=(1-t)*(1-t)*p0+2t(1-t)*p1+t*t*p2
     */
    private int getCurvePoint(int start, int control, int end) {
        return (int) ((1.0 - t) * (1.0 - t) * start + 2.0 * t * (1.0 - t) * control + t * t * end);
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 获取屏幕的宽度
     *
     * @return
     */
    private int getScreenWidth() {
        WindowManager wm = (WindowManager) getContext().getSystemService(
                Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.widthPixels;
    }

    /**
     * 获取屏幕的高度
     */
    private int getScreenHeight() {
        WindowManager wm = (WindowManager) getContext().getSystemService(
                Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.heightPixels;
    }

    public void firstLoadAniomation() {
        moveAnimation(
                0, 0, mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                mList.get(3).getCoordinateX() - mList.get(2).getCoordinateX() + mList.get(2).getCoordinateX()
                        - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                mList.get(3).getCoordinateY() - mList.get(2).getCoordinateY() + mList.get(2).getCoordinateY()
                        - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                mList.get(2).getCoordinateX() - mList.get(1).getCoordinateX() - VIEWWIDTH / 2 + mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX(),
                mList.get(2).getCoordinateY() - mList.get(1).getCoordinateY() + mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY(),
                mList.get(1).getCoordinateX() - mList.get(0).getCoordinateX() - VIEWWIDTH / 2, mList.get(1).getCoordinateY() - mList.get(0).getCoordinateY()
        );
    }

}
