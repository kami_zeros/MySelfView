package com.zeros.beiserqq.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.zeros.beiserqq.R;

/**
 * 曲线- ▏2018/5/3.
 */

public class RewardBackgroundView extends View {

    private Paint paint;
    private Path path;
    private float cx, cy, ex;
    private float dy;

    public RewardBackgroundView(Context context) {
        super(context);
        init(context);
    }

    public RewardBackgroundView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RewardBackgroundView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(21)
    public RewardBackgroundView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(ContextCompat.getColor(context, R.color.colorAccent));
        path = new Path();
    }

//    绘制二级 贝瑟尔曲线
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        path.moveTo(0, dy);       //移动画笔
//        path.quadTo(cx, cy, ex, dy);//quadTo 用于绘制圆滑曲线，即贝塞尔曲线。 (x1,y1) 为控制点，(x2,y2)为结束点。
        path.quadTo(cx, 0, ex, dy);//quadTo 用于绘制圆滑曲线，即贝塞尔曲线。 (x1,y1) 为控制点，(x2,y2)为结束点。
        path.lineTo(ex, 0);     //画直线，默认从(0,0)开始，但是此处画笔被移动到(0,dy)
        path.lineTo(0, 0);
        path.close();
        canvas.drawPath(path, paint);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        cx = w / 2.0f;
        cy = h;
        ex = w;
        dy = h / 1.8f;
        Log.e("Tag--", w + "-" + cx + "-" + cy);
    }

}
