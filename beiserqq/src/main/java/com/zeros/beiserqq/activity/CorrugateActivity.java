package com.zeros.beiserqq.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.zeros.beiserqq.R;
import com.zeros.beiserqq.widget.CorrugateView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 波浪的头像
 */
public class CorrugateActivity extends AppCompatActivity {

    @BindView(R.id.cv_waves)
    CorrugateView cv_waves;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corrugate);
        ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cv_waves.cancelTask();
    }
}
