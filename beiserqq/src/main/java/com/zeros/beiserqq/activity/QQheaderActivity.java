package com.zeros.beiserqq.activity;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Slide;
import android.view.Window;

import com.zeros.beiserqq.R;

public class QQheaderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setEnterTransition(new Explode());
            window.setExitTransition(new Slide());
        }
        setContentView(R.layout.activity_qqheader);
    }
}
