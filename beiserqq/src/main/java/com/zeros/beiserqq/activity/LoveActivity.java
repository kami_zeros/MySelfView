package com.zeros.beiserqq.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.zeros.beiserqq.R;
import com.zeros.beiserqq.widget.LoveLayout;

/**
 * 点击出现爱心
 */
public class LoveActivity extends AppCompatActivity {

    private LoveLayout loveLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_love);
        loveLayout = (LoveLayout)findViewById(R.id.loveLayout);
    }

    public void start(View view){
        loveLayout.addLove();
    }

}
