package com.zeros.beiserqq.header;

import android.widget.AbsListView;

/**
 * ▏2018/5/2.
 */

public interface ScrollTabHolder {

    void adjustScroll(int scrollHeight);

    void onScroll(AbsListView view, int firstVisibleItem,
                  int visibleItemCount, int totalItemCount, int pagePosition);

}
