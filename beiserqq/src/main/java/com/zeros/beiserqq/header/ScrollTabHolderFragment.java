package com.zeros.beiserqq.header;

import android.support.v4.app.Fragment;

/**
 * ▏2018/5/2.
 */

public abstract class ScrollTabHolderFragment extends Fragment implements ScrollTabHolder{

    protected ScrollTabHolder mScrollTabHolder;

    public void setScrollTabHolder(ScrollTabHolder mScrollTabHolder) {
        this.mScrollTabHolder = mScrollTabHolder;
    }
}
