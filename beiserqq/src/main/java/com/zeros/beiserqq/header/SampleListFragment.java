package com.zeros.beiserqq.header;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.zeros.beiserqq.R;
import com.zeros.beiserqq.activity.HeaderMainActivity;

import java.util.ArrayList;

/**
 * ▏2018/5/2.
 */

public class SampleListFragment extends ScrollTabHolderFragment {

    private static final String ARG_POSITION = "position";
    private ListView mListView;
    private ArrayList<String> mListItems;
    private int mPosition;

    //传递数据
    public static Fragment newInstance(int position) {
        SampleListFragment fragment = new SampleListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_POSITION, position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(ARG_POSITION);
        mListItems = new ArrayList<>();
        for (int i = 0; i <= 100; i++) {
            mListItems.add(i + ".item - current page:" + (mPosition + 1));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, null);
        mListView = view.findViewById(R.id.listView);

        View placeHolderView = inflater.inflate(R.layout.view_header_placeholder, mListView, false);
        placeHolderView.setBackgroundColor(0xffffffff);
        mListView.addHeaderView(placeHolderView);

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListView.setOnScrollListener(new OnScroll());
        mListView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_list, android.R.id.text1, mListItems));

        //in my moto phone(android 2.1),setOnScrollListener do not work well
        if (HeaderMainActivity.NEEDS_PROXY) {
            mListView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (mScrollTabHolder != null) {
                        mScrollTabHolder.onScroll(mListView, 0, 0, 0, mPosition);
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public void adjustScroll(int scrollHeight) {
        if (scrollHeight == 0 && mListView.getFirstVisiblePosition() >= 1) {
            return;
        }
        mListView.setSelectionFromTop(1, scrollHeight);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount, int pagePosition) {

    }


    public class OnScroll implements AbsListView.OnScrollListener{

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (mScrollTabHolder != null) {
                mScrollTabHolder.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount, mPosition);
            }
        }
    }





}
