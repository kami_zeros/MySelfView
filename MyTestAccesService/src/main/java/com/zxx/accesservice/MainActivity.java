package com.zxx.accesservice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * 这个应用是为了让 MyScannerGun 开启AccessService监听使用的
 * 另外：又写了测试 在当前界面没有EditText情况下（没有焦点）获取扫描内容
 */
public class MainActivity extends AppCompatActivity {

    private TextView tvFocus;
    private EditText etAll;
    private String mCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //这个是测试 MyScannerGun的服务的
        findViewById(R.id.btn_click).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Tag--", "onClick: 我被点击了！！！");
            }
        });


        //------------------------------
        tvFocus = findViewById(R.id.tv_focus);
        etAll = findViewById(R.id.et_all);

        initView();
    }

    private void initView() {
        final String focus = tvFocus.getText().toString();
        Log.e("Tag-", focus);

        tvFocus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Tag-00:", tvFocus.getText().toString());
            }
        });

        tvFocus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.e("Tag---2:", editable.toString());
            }
        });


        etAll.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mCode = editable.toString();
                Log.e("Tag---3:", editable.toString());
                intText();
            }
        });
    }

    private void intText() {
        tvFocus.setText(mCode);
    }


}
