package com.mukj.myspann;

import android.graphics.Color;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * setSpan(Object what, int start, int end, int flags)
 * what ： 文本格式，可以设置成前景色，背景色，下划线，中划线，模糊等
 * start ： 字符串设置格式的起始下标
 * end ： 字符串设置格式结束下标
 * flags ： 标识
 * <p>
 * flags 常用的几种属性：
 * Spanned.SPAN_INCLUSIVE_EXCLUSIVE 从起始下标到结束下标，包括起始下标不包含结束坐标
 * Spanned.SPAN_EXCLUSIVE_EXCLUSIVE 从起始下标到结束下标，但都不包括起始下标和结束下标
 * Spanned.SPAN_INCLUSIVE_INCLUSIVE 从起始下标到终了下标，同时包括起始下标和结束下标
 * Spanned.SPAN_EXCLUSIVE_INCLUSIVE 从起始下标到终了下标，包括结束下标不包含起始坐标
 *
 * @author zqq on 2019/5/15.
 */
public class MySpanned {

    //一、TextView点击获取部分内容

    //获取段落
    public static void getEachParagraph(TextView textView) {
        Spannable spans = (Spannable) textView.getText();
        Integer[] indices = getIndices(textView.getText().toString().trim(), ',');
        int start = 0;
        int end = 0;
        for (int i = 0; i <= indices.length; i++) {
            ClickableSpan clickSpan = getClickableSpan();
            end = (i < indices.length ? indices[i] : spans.length());
            spans.setSpan(clickSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            start = end + 1;
        }
        //改变选中文本的高亮颜色
        textView.setHighlightColor(Color.RED);
    }

    //click
    private static ClickableSpan getClickableSpan() {
        return new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                TextView tv = (TextView) widget;
                String s = tv.getText().subSequence(tv.getSelectionStart(), tv.getSelectionEnd()).toString();
                Log.e("-span-click-:", s);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
                ds.setUnderlineText(false);
            }
        };
    }

    //array
    private static Integer[] getIndices(String text, char split) {
        int pos = text.indexOf(split, 0);
        List<Integer> indices = new ArrayList<>();
        while (pos != -1) {
            indices.add(pos);
            pos = text.indexOf(split, pos + 1);
        }
        return indices.toArray(new Integer[0]);
    }

//    ----------------------------------


}
