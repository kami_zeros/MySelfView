package com.mukj.myspann;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.provider.Browser;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.MaskFilterSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView textView, textView2, textView3, textView4, textView5, textView6, textView7, textView8;
    private TextView textView51, textView61, textView9, textView10;
    private TextView textView11, textView12, textView13, textView131, textView132, textView133;
    private TextView textView14, textView15;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String id = DevicesUtil.getDeviceId();
        String mac = DevicesUtil.getPadIMEI();
        Log.e("id:", id);
        Log.e("id-mac:", mac);

//        String有StringBuilder用于字符串的拼接；
//        那么SpannableString也有SpannableStringBuilder用于拼接SpannableString，可以把各种风格效果拼接在一起。

        initView();
        spanParagraph();
        spanForeColor();
        spanBackColor();
        spanRelative();
        spanStrike1();
        spanStrike2();
        spanUnder1();
        spanUnder2();
        spanSupper();
        spanSub();
        spanStyle();
        spanImage();
        spanClick();
        spanURL();
        spanBlur();
        spanEmboss();
    }

    private void initView() {
        textView = findViewById(R.id.text1);
        textView2 = findViewById(R.id.text2);
        textView3 = findViewById(R.id.text3);
        textView4 = findViewById(R.id.text4);
        textView5 = findViewById(R.id.text5);
        textView51 = findViewById(R.id.text51);
        textView61 = findViewById(R.id.text61);
        textView6 = findViewById(R.id.text6);
        textView7 = findViewById(R.id.text7);
        textView8 = findViewById(R.id.text8);
        textView9 = findViewById(R.id.text9);
        textView10 = findViewById(R.id.text10);
        textView11 = findViewById(R.id.text11);
        textView12 = findViewById(R.id.text12);
        textView13 = findViewById(R.id.text13);
        textView131 = findViewById(R.id.text131);
        textView132 = findViewById(R.id.text132);
        textView133 = findViewById(R.id.text133);
        textView14 = findViewById(R.id.text14);
        textView15 = findViewById(R.id.text15);
    }

    //一、TextView点击获取部分内容
    private void spanParagraph() {
        textView.setText(getResources().getString(R.string.text1), TextView.BufferType.SPANNABLE);
        MySpanned.getEachParagraph(textView);
    }

//    -------------------------------

    //二、前景色
    private void spanForeColor() {
        SpannableString spannableString = new SpannableString("设置文字的前景色");
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#0099ee"));
        spannableString.setSpan(colorSpan, 5, spannableString.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        textView2.setText(spannableString);
    }

    //    -------------------------------

    //三、背景色
    private void spanBackColor() {
        SpannableString spannableString = new SpannableString("设置文字的背景色");
        BackgroundColorSpan colorSpan = new BackgroundColorSpan(Color.parseColor("#AC00FF30"));
        spannableString.setSpan(colorSpan, 5, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView3.setText(spannableString);
    }

    //    -------------------------------

    //四、一行文字不同样式
    private void spanRelative() {
        SpannableString spannableString = new SpannableString("9月22日");
        RelativeSizeSpan sizeSpan = new RelativeSizeSpan(2.0f);
        spannableString.setSpan(sizeSpan, 2, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView4.setText(spannableString);
    }

    //    -------------------------------

    //五、中划线
    private void spanStrike1() {
        SpannableString spannableString = new SpannableString("我是文本中划线");
        StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
        spannableString.setSpan(strikethroughSpan, 3, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView5.setText(spannableString);
    }

    private void spanStrike2() {
        textView51.setText("198￥");
        textView51.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        //textView6.getPaint().setFlags(0); // 取消设置的的划线
    }

    //    -------------------------------

    //六、下划线
    private void spanUnder1() {
        SpannableString spannableString = new SpannableString("我是文本下划线");
        UnderlineSpan underlineSpan = new UnderlineSpan();
        spannableString.setSpan(underlineSpan, 4, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView6.setText(spannableString);
    }

    private void spanUnder2() {
        textView61.setText("198￥");
        textView61.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        //textView61.getPaint().setFlags(0); // 取消设置的的划线
    }

    //    -------------------------------

    //七、上标
    private void spanSupper() {
        SpannableString spannableString = new SpannableString("你有新消息了" + "●");
        SuperscriptSpan superscriptSpan = new SuperscriptSpan();
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.RED);
        spannableString.setSpan(colorSpan, 6, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(superscriptSpan, 6, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView7.setText(spannableString);
    }

    //    -------------------------------

    //八、下标
    private void spanSub() {
        SpannableString spannableString = new SpannableString("注释1");
        SubscriptSpan subscriptSpan = new SubscriptSpan();
        spannableString.setSpan(subscriptSpan, 2, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView8.setText(spannableString);
    }

    //    -------------------------------

    //九、设置风格（粗体、斜体）StyleSpan
    private void spanStyle() {
        SpannableString spannableString = new SpannableString("为文字设置粗体,斜体风格");
        StyleSpan styleSpan_I = new StyleSpan(Typeface.ITALIC);
        StyleSpan styleSpan_B = new StyleSpan(Typeface.BOLD);

        spannableString.setSpan(styleSpan_B, 5, 7, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(styleSpan_I, 8, 10, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView9.setText(spannableString);
    }

    //    -------------------------------

    //十、图片
    private void spanImage() {
        SpannableString spannableString = new SpannableString("在文本中添加xx");
        Drawable drawable = getResources().getDrawable(R.drawable.ic_launcher_foreground);
        int drawHeight = drawable.getMinimumHeight();
        //setBounds方法设置图片的边框大小，使图片与文本居中对齐。
        drawable.setBounds(0, 0, drawHeight, drawHeight);

        ImageSpan imageSpan = new ImageSpan(drawable);

        spannableString.setSpan(imageSpan, 6, 8, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView10.setText(spannableString);
    }

    //    -------------------------------

    //十一、点击响应
    private void spanClick() {
        SpannableString spannableString = new SpannableString("为文字设置点击事件");
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Snackbar.make(widget, "你点击了我", Snackbar.LENGTH_SHORT).show();
                Uri uri = Uri.parse("http://www.imengu.cn");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, MainActivity.this.getPackageName());

                try {
                    MainActivity.this.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.e("--ClickableSpan--", "Actvity was not found for intent, " + intent.toString());
                }
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.YELLOW);
                ds.setUnderlineText(true);
            }
        }, 5, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        textView11.setMovementMethod(LinkMovementMethod.getInstance());
        textView11.setHighlightColor(Color.parseColor("#ff0000"));
        textView11.setText(spannableString);
    }

    //    -------------------------------

    //十二、超链接
    private void spanURL() {
        SpannableString spannableString = new SpannableString("为文字设置超链接");

        URLSpan urlSpan = new URLSpan("http://www.imengu.cn");
        spannableString.setSpan(urlSpan, 5, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView12.setMovementMethod(LinkMovementMethod.getInstance());
        //点击时背景色
        textView12.setHighlightColor(Color.parseColor("#0000ff"));
        textView12.setText(spannableString);
    }

    //    -------------------------------

    //十三、模糊
    private void spanBlur() {
        SpannableString span1 = new SpannableString("为文字为文字为文字");
        MaskFilterSpan maskFilterSpan = new MaskFilterSpan(new BlurMaskFilter(5, BlurMaskFilter.Blur.NORMAL));
        span1.setSpan(maskFilterSpan, 0, 2, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView13.setText(span1);

        SpannableString spannableString2 = new SpannableString("为文字设置模糊");
        MaskFilterSpan mask2 = new MaskFilterSpan(new BlurMaskFilter(10, BlurMaskFilter.Blur.INNER));
        spannableString2.setSpan(mask2, 2, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView131.setText(spannableString2);

        SpannableString spannableString3 = new SpannableString("为文字设置模糊");
        MaskFilterSpan mask3 = new MaskFilterSpan(new BlurMaskFilter(10, BlurMaskFilter.Blur.OUTER));
        spannableString3.setSpan(mask3, 4, 6, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView132.setText(spannableString3);
//
        SpannableString spannableString4 = new SpannableString("为文字设置模糊");
        MaskFilterSpan mask4 = new MaskFilterSpan(new BlurMaskFilter(10, BlurMaskFilter.Blur.SOLID));
        spannableString4.setSpan(mask4, 5, spannableString4.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView133.setText(spannableString4);

//        BlurMaskFilter(float radius, Blur style)
//        radius 模糊半径
//        style 风格
//        BlurMaskFilter.Blur.NORMAL 默认类型，模糊内外边界
//        BlurMaskFilter.Blur.INNER 内部模糊
//        BlurMaskFilter.Blur.OUTER 外部模糊
//        BlurMaskFilter.Blur.SOLID 在边界内绘制固体，模糊
    }

    //    -------------------------------

    //十四、浮雕
    private void spanEmboss() {
        SpannableString spannableString = new SpannableString("为文字设置浮雕");
        MaskFilterSpan maskFilterSpan = new MaskFilterSpan(
                new EmbossMaskFilter(new float[]{10, 10, 10}, 0.5f, 1f, 1f));
        spannableString.setSpan(maskFilterSpan, 5, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView14.setText(spannableString);
//        public EmbossMaskFilter(float[] direction, float ambient, float specular, float blurRadius)
//        direction 是float数组，定义长度为3的数组标量[x,y,z]，来指定光源的方向
//        ambient 取值在0到1之间，定义背景光 或者说是周围光
//        specular 定义镜面反射系数。
//        blurRadius 模糊半径。
    }

    //    -------------------------------------
    //十五、光栅
    private void spanLayer() {
//        SpannableString spannableString = new SpannableString("为文字设置光栅");
//        LayerRasterizer layerRasterizer = new LayerRasterizer();
//        layerRasterizer.addLayer(new Paint(Color.CYAN), 10, 10);
//        RasterizerSpan rasterizerSpan = new RasterizerSpan(layerRasterizer);
//        spannableString.setSpan(rasterizerSpan, 5, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
//        textView15.setText(spannableString);
    }

}
