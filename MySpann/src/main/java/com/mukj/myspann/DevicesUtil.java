package com.mukj.myspann;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

/**
 * 获取设备唯一标识
 *
 * @author zqq on 2019/5/15.
 */
public class DevicesUtil {

    //方法一（好像有重复的）
    public static String getDeviceId() {
        TelephonyManager tm = (TelephonyManager) MainApplication.getApp().getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (ActivityCompat.checkSelfPermission(MainApplication.getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return "权限缺失";
            }
            String imei = tm.getImei();
            if (!TextUtils.isEmpty(imei)) return imei;

            String meid = tm.getMeid();
            return TextUtils.isEmpty(meid) ? "" : meid;
        }
        return tm.getDeviceId();
    }

    //获取mac地址
    public static String getPadIMEI() {
        String macSerial = null;
        String str = "";
        try {
            Process pp = Runtime.getRuntime().exec("cat /sys/class/net/wlan0/address");
            InputStreamReader is = new InputStreamReader(pp.getInputStream());
            LineNumberReader lr = new LineNumberReader(is);
            for (; null != str; ) {
                str = lr.readLine();
                if (str != null) {
                    macSerial = str.trim();
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return macSerial == null ? "" : macSerial;
    }
}
