package com.mukj.myspann;

import android.app.Application;
import android.content.Context;

/**
 * @author zqq on 2019/5/15.
 */
public class MainApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    //
    public static Context getApp() {
        return context.getApplicationContext();
    }

    //
    public static Context getContext(){
        return context;
    }

}
