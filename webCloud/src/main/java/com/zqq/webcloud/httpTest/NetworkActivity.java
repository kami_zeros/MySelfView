package com.zqq.webcloud.httpTest;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.zqq.webcloud.R;

public class NetworkActivity extends AppCompatActivity implements NetworkAsyncTask.setResponseListener {

    private NetworkAsyncTask networkAsyncTask;
    private TextView tvUrl = null;
    private TextView tvRequestHeader = null;
    private TextView tvRequestBody = null;
    private TextView tvResponseHeader = null;
    private TextView tvResponseBody = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);

        tvUrl = (TextView) findViewById(R.id.tvUrl);
        tvRequestHeader = (TextView) findViewById(R.id.tvRequestHeader);
        tvRequestBody = (TextView) findViewById(R.id.tvRequestBody);
        tvResponseHeader = (TextView) findViewById(R.id.tvResponseHeader);
        tvResponseBody = (TextView) findViewById(R.id.tvResponseBody);

//        -------------------------------------------------------------------------
        networkAsyncTask = new NetworkAsyncTask(this, this);

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            String networkAction = intent.getStringExtra("action");
            networkAsyncTask.execute(networkAction);
        }
    }


    @Override
    public void setUrlHeader(String url, String requestHeader, String responseHeader) {
        tvUrl.setText(url);
        tvRequestHeader.setText(requestHeader);
        tvResponseHeader.setText(responseHeader);
    }

    @Override
    public void setBody(String body) {
        tvRequestBody.setText(body);
    }

    @Override
    public void setResponse(String response) {
        tvResponseBody.setText(response);
    }

}
