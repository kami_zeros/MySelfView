package com.zqq.webcloud.httpTest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.zqq.webcloud.R;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener {

    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        btn1 = findViewById(R.id.button);
        btn2 = findViewById(R.id.button2);
        btn3 = findViewById(R.id.button3);
        btn4 = findViewById(R.id.button4);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, NetworkActivity.class);
        switch (v.getId()) {
            case R.id.button:
                intent.putExtra("action", "NETWORK_GET");
                break;
            case R.id.button2:
                intent.putExtra("action", "NETWORK_POST_KEY_VALUE");
                break;
            case R.id.button3:
                intent.putExtra("action", "NETWORK_POST_XML");
                break;
            case R.id.button4:
                intent.putExtra("action", "NETWORK_POST_JSON");
                break;
        }
        startActivity(intent);
    }
}
