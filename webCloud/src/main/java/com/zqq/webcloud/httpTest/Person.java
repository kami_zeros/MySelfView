package com.zqq.webcloud.httpTest;

/**
 * @author zqq on 2018/9/10
 */
public class Person {

    private String id = "";
    private String name = "";
    private int age = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("name:").append(getName()).append(", age:").append(getAge()).toString();
    }

}
