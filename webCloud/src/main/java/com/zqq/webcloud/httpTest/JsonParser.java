package com.zqq.webcloud.httpTest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 对XML数据进行解析
 *
 * @author zqq on 2018/9/10
 */
public class JsonParser {

    //
    public static List<Person> parse(String jsonStr){
        List<Person> persons = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray jsonArray = jsonObject.getJSONArray("persons");
            int length = jsonArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject personObject = jsonArray.getJSONObject(i);
                String id = personObject.getString("id");
                String name = personObject.getString("name");
                int age = personObject.getInt("age");

                Person person = new Person();
                person.setId(id);
                person.setName(name);
                person.setAge(age);
                persons.add(person);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return persons;
    }


}
