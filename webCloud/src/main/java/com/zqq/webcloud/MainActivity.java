package com.zqq.webcloud;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 这是一种------
 */
public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private String webUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = findViewById(R.id.web_view);
        initWebView();

        setGet();
//        setPost();
    }

    private void initWebView() {
        WebSettings webSettings = webView.getSettings();
    }

    @Override
    protected void onResume() {
        super.onResume();
        webView.onResume();
        webView.resumeTimers();
    }

    @Override
    protected void onPause() {
        super.onPause();
        webView.onPause();
        webView.pauseTimers();
    }

    private void setGet() {
        // GET请求
        String url = "https://leancloud.cn:443/1.1/classes/urls?limit=4";
        NetworkTask networkTask = new HttpURLConnectionNetworkTask(NetworkTask.GET);
//        传入参数并执行任务
        networkTask.execute(url);

        networkTask.setResponceLintener(new NetworkTask.ResponceLintener() {
            @Override
            public void onSuccess(String result) {
                if (result != null) {
                    Log.e("---Tag---", result);

                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        JSONArray jsonArray = jsonObject.getJSONArray("results");
                        JSONObject result1 = jsonArray.getJSONObject(0);
                        webUrl = result1.getString("url");

                        setWebClient();
                        Log.e("---Tag---", webUrl);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(String error) {
                Log.e("--Tag--", error);
            }
        });

    }

    //加载网页
    private void setWebClient() {
        webView.loadUrl(webUrl);

        //辅助 WebView 处理 Javascript 的对话框,网站图标,网站标题等等。
        webView.setWebChromeClient(new WebChromeClient());

        //webview拦截事件，处理各种通知 & 请求事件
        webView.setWebViewClient(new WebViewClient() {
            //加载url，防止调用系统浏览器加载
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }
        });

    }

    private void setPost() {
        //POST请求
        NetworkTask task = new HttpURLConnectionNetworkTask(NetworkTask.POST);
        task.execute("http://192.168.0.23:8091/post/usercancle", "postId=75&userId=3296");
        task.setResponceLintener(new NetworkTask.ResponceLintener() {
            @Override
            public void onSuccess(String result) {
                Log.e("--Tag--", result);
            }

            @Override
            public void onError(String error) {
                Log.e("--Tag--", error);
            }
        });
    }


    /**
     * 点击返回上一页面而不是退出浏览器
     * //此方法兼容Android 1.0到Android 2.1 也是常规方法
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (webView != null) {
            webView.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            webView.clearHistory();
            ((ViewGroup) webView.getParent()).removeView(webView);
            webView.destroy();
            webView = null;
        }
    }
}
