package com.zqq.webcloud;

import java.io.IOException;
import java.util.Map;

/**
 * Android 6.0后删除了
 * 以HttpClient访问网络AsyncTask,访问网络在子线程进行并返回主线程通知访问的结果
 * 步骤：
 * 1.创建HttpClient对象；
 * 2.创建请求的对象，如果为GET请求，则创建HttpGet对象，如果为POST请求，则创建HttpPost对象；
 * 3.调用HttpClient对象的execute发送请求，执行该方法后，将获得服务器返回的HttpResponse对象；
 * 4.检查相应状态是否正常；
 * 5.获得相应对象当中的数据。
 *
 * @author zqq on 2018/9/6
 */
public class HttpClientNetworkTask extends NetworkTask {

    public HttpClientNetworkTask(String method) {
        super(method);
    }

    @Override
    public String doGet(String url) {
        String result = null;
//        需要导入httpClient包
//        HttpClient httpCient = new DefaultHttpClient();
//        HttpGet httpGet = new HttpGet(url);
//        try {
//            HttpResponse httpResponse = httpCient.execute(httpGet);
//            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//                HttpEntity entity = httpResponse.getEntity();
//                result = EntityUtils.toString(entity,"utf-8");
//            } else {
//                isSuccess = false;
//                result = "网络响应状态码不为200!";
//            }
//        } catch (IOException e) {
//            isSuccess = false;
//            result = "网络访问错误:" + e.getMessage();
//        }

        return result;
    }

    @Override
    public String doPost(String url, Map<String, String> paramMap) {
        String result = null;
//        HttpClient httpCient = new DefaultHttpClient();
//        HttpPost httpRequest = new HttpPost(url);
//        // 使用NameValuePair来保存要传递的Post参数
//        List<NameValuePair> params = new ArrayList<NameValuePair>();
//        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
//            params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
//        }
//        try {
//            httpRequest.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
//            HttpResponse response = httpCient.execute(httpRequest);
//            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//                result = EntityUtils.toString(response.getEntity());
//            } else {
//                isSuccess = false;
//                result = "网络响应状态码不为200!";
//            }
//        } catch (IOException e) {
//            isSuccess = false;
//            result = "网络访问错误:" + e.getMessage();
//        }

        return result;
    }
}
