package com.zxx.mytip;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
//import android.support.annotation.IdRes;
//import android.support.annotation.LayoutRes;
//import android.support.annotation.StringRes;
//import android.support.annotation.StyleRes;
import android.view.View;
import android.view.animation.Animation;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;

import timber.log.Timber;

/**
 * @author zxx on 2019/7/29
 */
public class Tooltip2 {

    private Tooltip2(Context context, Builder builder) {

    }


    class Builder {

        Context context;
        Point point;
        ClosePolicy closePolicy = ClosePolicy.TOUCH_INSIDE_CONSUME;
        CharSequence text;
        View anchorView;
        Integer maxWidth;
        int defStyleRes = R.style.ToolTipLayoutDefaultStyle;
        int defStyleAttr = R.attr.ttlm_defaultStyle;
        Typeface typeface;
        boolean overlay = true;
        Animation floatingAnimation;
        Long showDuration;
        boolean showArrow = true;
        long activateDelay = 0L;
        boolean followAnchor = false;
        Integer animationStyle;

        Integer layoutId;
        Integer textId;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder typeface(Typeface value) {
            this.typeface = value;
            return this;
        }

        public Builder styleId(@StyleRes Integer styleId) {
            if (styleId != null) {
                this.defStyleRes = styleId;
            } else {
                this.defStyleRes = R.style.ToolTipLayoutDefaultStyle;
            }
            this.defStyleAttr = R.attr.ttlm_defaultStyle;
            return this;
        }

        public Builder customView(@LayoutRes Integer layoutId, @IdRes Integer textId) {
            this.layoutId = layoutId;
            this.textId = textId;
            return this;
        }

        public Builder activateDelay(Long value) {
            this.activateDelay = value;
            return this;
        }

        public Builder arrow(boolean value) {
            this.showArrow = value;
            return this;
        }

        public Builder showDuration(long value) {
            this.showDuration = value;
            return this;
        }

        public Builder floatingAnimation(Animation value) {
            this.floatingAnimation = value;
            return this;
        }

        public Builder maxWidth(Integer value) {
            this.maxWidth = value;
            return this;
        }

        public Builder overlay(boolean value) {
            this.overlay = value;
            return this;
        }

        public Builder anchor(Integer x, Integer y) {
            this.anchorView = null;
            this.point = new Point(x, y);
            return this;
        }

        public Builder anchor(View view, Integer xoff, Integer yoff, boolean follow) {
            this.anchorView = view;
            this.followAnchor = follow;
            this.point = new Point(xoff, yoff);
            return this;
        }

        public Builder text(CharSequence value) {
            this.text = value;
            return this;
        }

        public Builder text(@StringRes int value) {
            this.text = context.getString(value);
            return this;
        }

        public Builder text(@StringRes int value, Object... args) {
            this.text = context.getString(value, args);
            return this;
        }

        public Builder closePolicy(ClosePolicy value) {
            this.closePolicy = value;
            Timber.v("closePolicy:" + value);
            return this;
        }

        public Builder animationStyle(@StringRes int id) {
            this.animationStyle = id;
            return this;
        }

        public Tooltip2 create() {
            if (null == anchorView && null == point) {
                throw new IllegalArgumentException("missing anchor point or anchor view");
            }
            return new Tooltip2(context, this);
        }
    }


}
