package com.zxx.mytip;

/**
 * @author zxx on 2019/7/29
 */
public class ClosePolicy {

    public static final int NONE = 0;
    public static final int TOUCH_INSIDE = 1 << 1;
    public static final int TOUCH_OUTSIDE = 1 << 2;
    public static final int CONSUME = 1 << 3;

    public static final ClosePolicy TOUCH_NONE = new ClosePolicy(NONE);
    public static final ClosePolicy TOUCH_INSIDE_CONSUME = new ClosePolicy(TOUCH_INSIDE | CONSUME);
    public static final ClosePolicy TOUCH_INSIDE_NO_CONSUME = new ClosePolicy(TOUCH_INSIDE);
    public static final ClosePolicy TOUCH_OUTSIDE_CONSUME = new ClosePolicy(TOUCH_OUTSIDE | CONSUME);
    public static final ClosePolicy TOUCH_OUTSIDE_NO_CONSUME = new ClosePolicy(TOUCH_OUTSIDE);
    public static final ClosePolicy TOUCH_ANYWHERE_NO_CONSUME = new ClosePolicy(TOUCH_INSIDE | TOUCH_OUTSIDE);
    public static final ClosePolicy TOUCH_ANYWHERE_CONSUME = new ClosePolicy(TOUCH_INSIDE | TOUCH_OUTSIDE | CONSUME);

    int policy;

    public ClosePolicy(int policy) {
        this.policy = policy;
    }

    public boolean consume() {
        return (policy & CONSUME) == CONSUME;
    }

    public boolean inside() {
        return (policy & TOUCH_INSIDE) == TOUCH_INSIDE;
    }

    public boolean outside() {
        return (policy & TOUCH_OUTSIDE) == TOUCH_OUTSIDE;
    }

    public boolean anywhere() {
        return inside() & outside();
    }

    @Override
    public String toString() {
        return "ClosePolicy{" +
                "policy:" + policy +
                ",inside:" + inside() +
                ",outside:" + outside() +
                ",anywhere:" + anywhere() +
                ",consume:" + consume() +
                '}';
    }

    class Builder {
        private int policy = NONE;

        public Builder consume(Boolean value) {
            if (value) {
                policy = policy | CONSUME;
            } else {
                policy = policy & ~CONSUME;
            }
            return this;
        }

        public Builder inside(Boolean value) {
            if (value) {
                policy = policy | TOUCH_INSIDE;
            } else {
                policy = policy & ~TOUCH_INSIDE;
            }
            return this;
        }

        public Builder outside(Boolean value) {
            if (value) {
                policy = policy | TOUCH_OUTSIDE;
            } else {
                policy = policy & ~TOUCH_OUTSIDE;
            }
            return this;
        }

        public void clear() {
            policy = NONE;
        }

        public ClosePolicy build() {
            return new ClosePolicy(policy);
        }
    }

}
