package com.zxx.mytip;

import android.graphics.Typeface;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import it.sephiroth.android.library.xtooltip.ClosePolicy;
import it.sephiroth.android.library.xtooltip.Tooltip;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import timber.log.Timber;


public class MainActivity extends AppCompatActivity {

    private TextView tvClick;
    private Tooltip tooltip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvClick = findViewById(R.id.tv_click);

        tooltip = new Tooltip.Builder(this)
                .anchor(tvClick, 0, 0, false)
                .text("不允许使用")
                .styleId(R.style.ToolTipLayoutDefaultStyle)
                .typeface(Typeface.DEFAULT)
                .maxWidth(400)
                .arrow(true)
                .floatingAnimation(Tooltip.Animation.Companion.getDEFAULT())
                .closePolicy(ClosePolicy.Companion.getTOUCH_OUTSIDE_CONSUME())
                .showDuration(1500)
                .overlay(false)
                .create();

        tooltip.doOnHidden(tooltip -> {
            tooltip = null;
            return null;
        }).doOnFailure(tooltip -> null)
                .doOnShown(tooltip -> null);


        tvClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Timber.e("gravity-->");
                tooltip.show(view, Tooltip.Gravity.RIGHT, true);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (tooltip != null) {
            tooltip.dismiss();
        }
    }
}
