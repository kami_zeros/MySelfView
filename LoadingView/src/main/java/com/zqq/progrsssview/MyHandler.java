package com.zqq.progrsssview;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

/**
 * @author zqq on 2018-11-21
 */
public class MyHandler extends Handler {

    WeakReference<Activity> weakReference;

    public MyHandler(Activity activity) {
        weakReference =new WeakReference<>(activity);
    }

    @Override
    public void handleMessage(Message msg) {
        Activity activity = weakReference.get();
        if (activity != null) {
            if (msg.what == 1) {
                //业务处理
                //adapter.notifyDataSetChanged();
            }
        }
    }
}
