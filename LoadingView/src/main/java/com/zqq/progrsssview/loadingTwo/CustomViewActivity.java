package com.zqq.progrsssview.loadingTwo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.zqq.progrsssview.R;

import java.util.ArrayList;
import java.util.List;

public class CustomViewActivity extends AppCompatActivity {

    protected ListView mListView;
    protected CommonLoadingView mLoadingView;
    private List<String> mList = new ArrayList<>();
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_view);
        initView();
    }

    private void initView() {
        mListView = (ListView) findViewById(R.id.listView);
        mLoadingView = (CommonLoadingView) findViewById(R.id.loadingView);

        LinearLayout linearLayout = findViewById(R.id.test);//1.可以
        View view = LayoutInflater.from(this).inflate(R.layout.common_loading_view, null);
        //设置自定义视图
        ProgressBar progressBar = new ProgressBar(this);
        this.mLoadingView.setLoadingView(view);


        TextView textView = new TextView(this);
        textView.setText("加载失败...");
        this.mLoadingView.setLoadingErrorView(textView);

        mLoadingView.load();

        //设置点击错误视图重新加载事件
        mLoadingView.setLoadingHandler(new CommonLoadingView.LoadingHandler() {
            @Override
            public void doRequestData() {
                mLoadingView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 1; i <=20 ; i++) {
                            mList.add(i+"");
                        }
                        adapter = new ArrayAdapter(CustomViewActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, mList);
                        mListView.setAdapter(adapter);
                        mLoadingView.loadSuccess(false);
                    }
                },2500);
            }
        });

        //模拟网络错误，加载失败
        mLoadingView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mLoadingView.loadError();
            }
        },2500);
    }

}
