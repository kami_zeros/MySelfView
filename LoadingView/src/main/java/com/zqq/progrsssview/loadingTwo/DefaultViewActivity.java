package com.zqq.progrsssview.loadingTwo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.zqq.progrsssview.R;

import java.util.ArrayList;
import java.util.List;

public class DefaultViewActivity extends AppCompatActivity {

    protected ListView mListView;
    protected CommonLoadingView mLoadingView;
    private List<String> mList = new ArrayList<>();
    ArrayAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_view);
        initView();
    }

    private void initView() {
        mListView = findViewById(R.id.listView);
        mLoadingView = findViewById(R.id.loadingView);
        mLoadingView.load();

        //设置点击错误视图重新加载事件
        mLoadingView.setLoadingHandler(new CommonLoadingView.LoadingHandler() {
            @Override
            public void doRequestData() {
                mLoadingView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 1; i <=20 ; i++) {
                            mList.add(i+"");
                        }
                        adapter = new ArrayAdapter(DefaultViewActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, mList);
                        mListView.setAdapter(adapter);
                        mLoadingView.loadSuccess(false);
                    }
                },2500);
            }
        });

        //模拟网络错误，加载失败
        mLoadingView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mLoadingView.loadError();
            }
        },2500);

    }


}
