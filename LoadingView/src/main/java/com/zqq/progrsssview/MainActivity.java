package com.zqq.progrsssview;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.zqq.progrsssview.loadingTwo.CustomViewActivity;
import com.zqq.progrsssview.loadingTwo.DefaultViewActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    protected Button mDefaultBtn;
    protected Button mCustomBtn;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.tv);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading(MainActivity.this);
            }
        });

        initView();
    }


    private LoadingDialog dialog, dg;

    //
    public void showLoading(Context context) {
        try {
            dg = new LoadingDialog(context);
            dg.setCancelable(true);
            dg.setCanceledOnTouchOutside(false);
            if (!dg.isShowing()) {
                dg.show();
            }
        } catch (Exception e) {
            dg = null;
        }
    }

    public void dismissLoad() {
        if (dg != null && dg.isShowing()) {
            try {
                dg.dismiss();
            } catch (Exception e) {
            }
            dg = null;
        }
    }

    //以下context取自某个activity
    public void showLoading(int flag) {
        try {
            if (dialog == null) {
//                dialog = new LoadingDialog(AppManager.getAppManager().currentActivity(), flag);
            }
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            if (!dialog.isShowing()) {
                dialog.show();
            }
        } catch (Exception e) {
            dialog = null;
        }
    }

    public void dismissLoading() {
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
            }
            dialog = null;
        }
    }


    //    ---------------loading View two --------------
    private void initView() {
        mDefaultBtn = (Button) findViewById(R.id.default_btn);
        mDefaultBtn.setOnClickListener(MainActivity.this);
        mCustomBtn = (Button) findViewById(R.id.custom_btn);
        mCustomBtn.setOnClickListener(MainActivity.this);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.default_btn) {
            Intent intent = new Intent(this, DefaultViewActivity.class);
            startActivity(intent);
        } else if (view.getId() == R.id.custom_btn) {
            Intent intent = new Intent(this, CustomViewActivity.class);
            startActivity(intent);
        }
    }
}
