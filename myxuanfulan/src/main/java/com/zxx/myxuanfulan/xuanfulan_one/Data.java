package com.zxx.myxuanfulan.xuanfulan_one;

import java.util.List;

/**
 * @author zqq on 2020/1/4.
 */
public class Data {

    private String name;
    private List<erData> erDatas;

    public Data() {
    }

    public Data(String name, List<erData> erDatas) {
        this.name = name;
        this.erDatas = erDatas;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<erData> getErDatas() {
        return erDatas;
    }

    public void setErDatas(List<erData> erDatas) {
        this.erDatas = erDatas;
    }

    public static class erData {
        private String ss;

        public erData() {
        }

        public erData(String ss) {
            this.ss = ss;
        }

        public String getSs() {
            return ss;
        }

        public void setSs(String ss) {
            this.ss = ss;
        }
    }
}
