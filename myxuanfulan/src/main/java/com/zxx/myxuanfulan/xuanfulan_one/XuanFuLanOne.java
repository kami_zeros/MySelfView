package com.zxx.myxuanfulan.xuanfulan_one;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.zxx.myxuanfulan.R;

import java.util.ArrayList;
import java.util.List;

public class XuanFuLanOne extends AppCompatActivity {

    private RecyclerView rv;
    private FrameLayout top;
    private List<Data> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xuanfulan_one);

        buildData();

        rv = findViewById(R.id.rv);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(RecyclerView.VERTICAL);

        rv.setLayoutManager(manager);
        FirstAdapter firstAdapter = new FirstAdapter(this, data);
        rv.setAdapter(firstAdapter);

        //RecyclerView滑动监听
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            FrameLayout topContainer = findViewById(R.id.top_title);
            int currentY;
            //这里的50和item_first布局文件中的TextView的高度一致
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    (int) dip2px(XuanFuLanOne.this, 50));
            FrameLayout preView = null;

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //分别设置两个锚点
                FrameLayout currentUpView = (FrameLayout) recyclerView.findChildViewUnder(0, 0);
                FrameLayout currentLowView = (FrameLayout) recyclerView.findChildViewUnder(0,
                        (int) dip2px(XuanFuLanOne.this, 50));

                // 初始化preView
                if (dy == 0) {
                    preView = currentUpView;
                }
                //当下移到临街点的时候，标题栏从原view中移到topContainer中
                if (currentLowView != preView || dy > 0) {
                    TextView tv = (TextView) topContainer.getChildAt(0);
                    if (null != tv) {
                        topContainer.removeView(tv);
                        params.gravity = Gravity.BOTTOM;
                        tv.setLayoutParams(params);
                        preView.addView(tv);
                        // 将preView替换成现有的currentLowView
                        preView = currentLowView;
                    }
                }
                // 当上移到临界点时
                if (currentUpView != preView || dy < 0) {
                    //上标题栏置底
                    TextView upTv = currentUpView.findViewById(R.id.item_text);
                    if (null != upTv) {
                        params.gravity = Gravity.BOTTOM;
                        upTv.setLayoutParams(params);
                    }

                    // 下标题栏置顶
                    TextView lowTv = (TextView) topContainer.getChildAt(0);

                    if (null != lowTv) {
                        topContainer.removeView(lowTv);
                        params.gravity = Gravity.TOP;
                        lowTv.setLayoutParams(params);
                        preView.addView(lowTv);
                    }
                    // 将preView替换成现有的currentUpView
                    preView = currentUpView;
                }

                //只有一种情况会让标题栏上浮，就是两个锚点下的view相同的时候
                if (currentUpView.equals(currentLowView)) {
                    TextView tv = (TextView) currentLowView.findViewById(R.id.item_text);
                    if (null != tv) {
                        currentUpView.removeView(tv);
                        params.gravity = Gravity.TOP;
                        tv.setLayoutParams(params);
                        topContainer.addView(tv);
                    }
                }
            }
        });
    }

    private void buildData() {
        data = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            List<Data.erData> temp = new ArrayList<>();
            for (int j = 0; j < 20; j++) {
                temp.add(new Data.erData("ssssss"));
            }
            data.add(new Data("我是标题：" + i, temp));
        }
    }


    //
    public static float dip2px(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (dpValue * scale) + 0.5f;
    }

}
