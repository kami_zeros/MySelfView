package com.zxx.myxuanfulan.xuanfulan_one;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zxx.myxuanfulan.R;

import java.util.List;

/**
 * @author zqq on 2020/1/4.
 */
public class SecondAdapter extends RecyclerView.Adapter<SecondAdapter.SecondHolder> {

    private Context context;
    private List<Data.erData> erDatas;

    public SecondAdapter(Context context, List<Data.erData> erDatas) {
        this.context = context;
        this.erDatas = erDatas;
    }

    @NonNull
    @Override
    public SecondHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item1_second, parent, false);
        return new SecondHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SecondHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return erDatas.size();
    }

    class SecondHolder extends RecyclerView.ViewHolder {
        public SecondHolder(View itemView) {
            super(itemView);
        }
    }

}
