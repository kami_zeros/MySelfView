package com.zxx.myxuanfulan.xuanfulan_one;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zxx.myxuanfulan.R;

import java.util.List;

/**
 * @author zqq on 2020/1/4.
 */
public class FirstAdapter extends RecyclerView.Adapter<FirstAdapter.FirstHolder> {

    private Context context;
    private List<Data> datas;

    public FirstAdapter(Context context, List<Data> datas) {
        this.context = context;
        this.datas = datas;
    }

    @NonNull
    @Override
    public FirstHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item1_first, parent, false);
        return new FirstHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FirstHolder holder, int position) {
        holder.setView(datas.get(position));
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    class FirstHolder extends RecyclerView.ViewHolder {
        TextView title;
        RecyclerView erv;

        public FirstHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_text);
            erv = itemView.findViewById(R.id.er_rv);

            GridLayoutManager manager = new GridLayoutManager(context, 4) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            };
            erv.setLayoutManager(manager);
        }

        //
        public void setView(Data data) {
            title.setText(data.getName());

            SecondAdapter secondAdapter = new SecondAdapter(context, data.getErDatas());
            erv.setAdapter(secondAdapter);
        }
    }

}

