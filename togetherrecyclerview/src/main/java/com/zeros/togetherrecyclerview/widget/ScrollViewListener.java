package com.zeros.togetherrecyclerview.widget;

/**
 * ▏2018/4/27.
 */

public interface ScrollViewListener<T> {
    void onScrollChanged(T scrollView, int x, int y);
}
