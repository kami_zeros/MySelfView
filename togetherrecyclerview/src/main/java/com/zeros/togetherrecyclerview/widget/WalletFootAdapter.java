package com.zeros.togetherrecyclerview.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zeros.togetherrecyclerview.R;

import java.util.ArrayList;
import java.util.List;

/**
 * ▏2018/4/27.
 */

public class WalletFootAdapter extends RecyclerView.Adapter<WalletFootAdapter.RecyclerHolder> {

    private Context mContext;
    private List<String> dataList = new ArrayList<>();

    public WalletFootAdapter(RecyclerView recyclerView) {
        this.mContext = recyclerView.getContext();
    }

    public void setData(List<String> dataList) {
        if (null != dataList) {
            this.dataList.clear();
            this.dataList.addAll(dataList);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_wallet_footer, parent, false);
        return new RecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerHolder holder, int position) {
//        holder.textView.setText(dataList.get(position));
        if (position % 2 == 1) {
            holder.itemView.setBackgroundColor(Color.CYAN);

        } else {
            holder.itemView.setBackgroundColor(Color.MAGENTA);
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class RecyclerHolder extends RecyclerView.ViewHolder {
        TextView textView;

        private RecyclerHolder(View itemView) {
            super(itemView);
//            textView = (TextView) itemView.findViewById(R.id.tv__id_item_layout);
        }
    }

}
