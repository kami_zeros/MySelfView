# MySelfView

#### 项目介绍
炫酷UI框架：https://blog.csdn.net/qq_36875339/article/details/77602890

自定义View：
1.加载等待：钟摆  
2.一些常用utils  
3.togetherRecyclerview:head与foot联动效果  
4.贝瑟尔曲线</br>  
  
学习网络访问：  
1.原始网络访问  
2.AsyncTask讲解  
3.HttpURLConnection讲解  
4.WebView加载网址

5.Android仿抖音加载框之两颗小球转动控件  
https://mp.weixin.qq.com/s?__biz=MzAxMTI4MTkwNQ==&mid=2650826249&idx=1&sn=a4cbe712a6cced9a3aa89de370bb352b&chksm=80b7b297b7c03b814598eabbba701e2147631bf19d757f437977589e964412e7fb56fa06cd94&mpshare=1&scene=23&srcid=0912yimK1vex8bMpnJTYSQFN#rd

https://github.com/CCY0122/douyinloadingview

6.loveClick：点击心形跳动

7.SlideEdit：recyclerview侧滑删除

8.RainyView：https://github.com/samlss/RainyView
下雨View（学习自定义view）

9.CircleProgress:圆形进度条，水平进度条，带文字进度条

10.PigPage:绘制小猪佩奇- https://github.com/princekin-f/Page

11.MySpann:String

12.MyClock:数字时钟-https://juejin.im/post/5cb53e93e51d456e55623b07 <br>
https://github.com/drawf/SourceSet
https://juejin.im/post/5d52aea86fb9a06ae61aad5b  (壁纸)

14.MyTip:https://github.com/sephiroth74/android-target-tooltip
高版本2.0.3/2.0.4用的是AndroidX在向下兼容方便有问题

17.MyTip2: * 仿QQ 长按显示 多种操作 按钮
           * https://github.com/cjh910521/NewTipView
           *
           * 还有一个：https://github.com/JeasonWong/FlipShare

Android炫酷的UI界面:https://blog.csdn.net/qq_36875339/article/details/77602890
13.MyButtonLogin: https://github.com/dmytrodanylyk/circular-progress-button

14.NumberButton:购物车商品数量、增加和减少控制按钮。
https://github.com/qinci/NumberButton

15.CustomViewGroup：动态添加控件并换行
https://www.jianshu.com/p/b908edbccd01

16.自定义TabLayout:可以改变下划线长短
https://www.jianshu.com/p/83922d08250b
1.MagicIndicator: https://github.com/hackware1993/MagicIndicator
2.FlycoTabLayout: https://github.com/H07000223/FlycoTabLayout

TC time-用MyTC