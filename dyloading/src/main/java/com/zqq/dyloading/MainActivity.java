package com.zqq.dyloading;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    DYLoadingView loadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadingView = findViewById(R.id.dy);
        loadingView.setRadius(16, 16, 2);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                loadingView.start();
                break;
            case R.id.button2:
                loadingView.stop();
                break;
            default:
                break;
        }
    }
}


    /*
    *
#### 可用属性
| 名称 | 对应xml属性   | 对应java方法| 默认值|
|--------|----------|----------|------|
|球1半径	 | radius1	|setRadius() |6dp  |
|球2半径	 | radius2	|setRadius() |6dp  |
|两球间隔  | gap	|setRadius() |0.8dp  |
|球1颜色	 | color1	|setColors() |0XFFFF4040  |
|球2颜色	 | color2	|setColors() |0XFF00EEEE  |
|叠加色	 | mixColor	|setColors() |0XFF000000  |
|从右往左移动时小球最大缩放倍数	 | rtlScale	|setScales() |0.7f  |
|从左往右移动时小球最大缩放倍数	 | ltrScale	|setScales() |1.3f  |
|一次移动动画时长	 | duration	|setDuration() |350ms  |
|一次移动动画后停顿时长	 | pauseDuration	|setDuration() |80ms  |
|动画进度在[0,scaleStartFraction]期间，小球大小逐渐缩放| scaleStartFraction	|setStartEndFraction() |0.2f  |
|动画进度在[scaleEndFraction,1]期间，小球大小逐渐恢复 | scaleEndFraction	|setStartEndFraction() |0.8f  |
    * */

