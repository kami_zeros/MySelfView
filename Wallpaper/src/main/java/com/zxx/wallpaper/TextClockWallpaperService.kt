package com.zxx.wallpaper

import android.graphics.Color
import android.graphics.Paint
import android.service.wallpaper.WallpaperService
import android.util.Log

/**
 * @author zxx on 2019/9/27
 */
class TextClockWallpaperService : WallpaperService() {
    override fun onCreateEngine(): Engine {
        return MyEngine()
    }

    inner class MyEngine : Engine() {
        //准备画笔
        private val mPaint = Paint().apply {
            this.color = Color.RED
            this.isAntiAlias = true
            this.textSize = 60f
            this.textAlign = Paint.Align.CENTER
        }

        override fun onVisibilityChanged(visible: Boolean) {
            super.onVisibilityChanged(visible)
            Log.e("clock-", "onVisibilityChanged >>> $visible")
            //只在壁纸显示的做绘制操作，这很重要！
            if (visible) {
                surfaceHolder.lockCanvas().let { canvas ->
                    //将原点移动到画布中心
                    canvas.save()
                    canvas.translate((canvas.width / 2).toFloat(), (canvas.height / 2).toFloat())

                    //绘制文字
                    canvas.drawText("Hello World", 0f, 0f, mPaint)

                    canvas.restore()
                    surfaceHolder.unlockCanvasAndPost(canvas)
                }
            }
        }
    }

}