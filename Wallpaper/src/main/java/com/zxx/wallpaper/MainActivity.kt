package com.zxx.wallpaper

import android.app.WallpaperManager
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

/**
 * 注意要用真机测试（否则报错）
 * IllegalArgumentException: Service Intent must be explicit: Intent { act=android.service.wallpaper.WallpaperService (has extras) }
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //无启动页面
        //setContentView(R.layout.activity_main)

//        startWallpaper.setOnClickListener {
        val intent = Intent().apply {
            action = WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER
            putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, ComponentName(
                    applicationContext, TextClockWallpaperService::class.java
            ))
        }
        startActivity(intent)
//        }
    }


}
