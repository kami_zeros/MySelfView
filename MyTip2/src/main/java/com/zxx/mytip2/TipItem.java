package com.zxx.mytip2;

import android.content.Context;
import android.graphics.Color;

/**
 * @author zxx on 2021/3/2
 */
public class TipItem {
    private String title;
    private int itemWidth;
    private int textColor = Color.WHITE;

    public TipItem(String title, Context context, int widthDp) {
        this.title = title;
        this.itemWidth = DensityUtil.dip2px(context, widthDp);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(int itemWidth) {
        this.itemWidth = itemWidth;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }
}
