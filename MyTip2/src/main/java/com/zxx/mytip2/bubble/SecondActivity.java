package com.zxx.mytip2.bubble;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Layout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.zxx.mytip2.R;

public class SecondActivity extends AppCompatActivity {

    private BubblePopupWindow leftTopWindow;
    private BubblePopupWindow rightTopWindow;
    private BubblePopupWindow leftBottomWindow;
    private BubblePopupWindow rightBottomWindow;
    private BubblePopupWindow centerWindow;

    LayoutInflater inflater;

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        leftTopWindow = new BubblePopupWindow(this);
        rightTopWindow = new BubblePopupWindow(this);
        leftBottomWindow = new BubblePopupWindow(this);
        rightBottomWindow = new BubblePopupWindow(this);
        centerWindow = new BubblePopupWindow(this);

        inflater = LayoutInflater.from(this);

        button = findViewById(R.id.myButton);
        button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                BubblePopupWindow popupWindow = new BubblePopupWindow(SecondActivity.this);

                popupWindow.show(v,Gravity.LEFT);
                return false;
            }
        });
    }

    public void leftTop(View view) {
        View bubbleView = inflater.inflate(R.layout.layout_popup_view, null);
        TextView tvContent = (TextView) bubbleView.findViewById(R.id.tvContent);
        tvContent.setText("HelloWorld");

        //设置Tip布局
        leftTopWindow.setBubbleView(bubbleView);

        //设置要显示的在哪个view上
        leftTopWindow.show(view, Gravity.BOTTOM);
    }

    public void rightTop(View view) {
        View bubbleView = inflater.inflate(R.layout.layout_popup_view, null);
        TextView tvContent = (TextView) bubbleView.findViewById(R.id.tvContent);
        tvContent.setText("Left");

        rightTopWindow.setBubbleView(bubbleView);
        rightTopWindow.show(view, Gravity.LEFT, view.getHeight()/2f);
    }

    public void leftBottom(View view) {
        View bubbleView = inflater.inflate(R.layout.layout_popup_view, null);
        leftBottomWindow.setBubbleView(bubbleView);
        leftBottomWindow.show(view);
    }

    public void rightBottom(View view) {
        View bubbleView = inflater.inflate(R.layout.layout_popup_view, null);
        rightBottomWindow.setBubbleView(bubbleView);
        rightBottomWindow.show(view, Gravity.RIGHT, 0);
    }

    public void center(View view) {
        View bubbleView = inflater.inflate(R.layout.layout_popup_view, null);
        centerWindow.setBubbleView(bubbleView);
        centerWindow.show(view, Gravity.BOTTOM, 0);
    }


}