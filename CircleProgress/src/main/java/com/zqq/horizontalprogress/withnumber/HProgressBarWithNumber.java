package com.zqq.horizontalprogress.withnumber;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.ProgressBar;

import com.zqq.circleprogress.R;

/**
 * https://github.com/hongyangAndroid/Android-ProgressBarWidthNumber
 *
 * @author zxx on 2019/12/5
 */
public class HProgressBarWithNumber extends ProgressBar {

    private static final int DEFAULT_TEXT_SIZE = 10;
    private static final int DEFAULT_TEXT_COLOR = 0XFFFC00D1;
    private static final int DEFAULT_COLOR_UNREACHED = 0xFFd3d6da;
    private static final int DEFAULT_HEIGHT_REACHED_BAR = 2;
    private static final int DEFAULT_HEIGHT_UNREACHED_BAR = 2;
    private static final int DEFAULT_SIZE_TEXT_OFFSET = 10;

    protected Paint mPaint = new Paint();     //painter of all drawing things
    private int mTextColor = DEFAULT_TEXT_COLOR;    //color of progress number
    private int mTextSize = sp2px(DEFAULT_TEXT_SIZE);   // size of text (sp)
    private int mTextOffset = dp2px(DEFAULT_SIZE_TEXT_OFFSET);//offset of draw progress
    protected int mReachedBarHeight = dp2px(DEFAULT_HEIGHT_REACHED_BAR);//height of reached progress bar
    protected int mReachedBarColor = DEFAULT_TEXT_COLOR;  //color of reached bar
    protected int mUnReachedBarColor = DEFAULT_COLOR_UNREACHED;//color of unreached bar
    protected int mUnReachedBarHeight = dp2px(DEFAULT_HEIGHT_UNREACHED_BAR);//height of unreached progress bar
    private int mRealWidth;       //view width except padding
    private boolean mIfDrawText = true;
    private static final int VISIBLE = 0;


    public HProgressBarWithNumber(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HProgressBarWithNumber(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
        mPaint.setTextSize(mTextSize);
        mPaint.setColor(mTextColor);
    }

    private void init(AttributeSet attrs) {
        TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.HorizontalProgressBarWithNumber);
        mTextColor = array.getColor(R.styleable.HorizontalProgressBarWithNumber_progressTextColor, DEFAULT_TEXT_COLOR);
        mTextSize = (int) array.getDimension(R.styleable.HorizontalProgressBarWithNumber_progressTextSize, mTextSize);
        mReachedBarColor = array.getColor(R.styleable.HorizontalProgressBarWithNumber_progressReachedColor, mTextColor);
        mReachedBarHeight = (int) array.getDimension(R.styleable.HorizontalProgressBarWithNumber_progressReachedBarHeight, mReachedBarHeight);
        mUnReachedBarColor = array.getColor(R.styleable.HorizontalProgressBarWithNumber_progressUnreachedColor, DEFAULT_COLOR_UNREACHED);
        mUnReachedBarHeight = (int) array.getDimension(R.styleable.HorizontalProgressBarWithNumber_progressUnreachedBarHeight, mUnReachedBarHeight);
        mTextOffset = (int) array.getDimension(R.styleable.HorizontalProgressBarWithNumber_progressTextOffset, mTextOffset);

        int textVisible = array.getInt(R.styleable.HorizontalProgressBarWithNumber_progressTextVisibility, VISIBLE);
        if (textVisible != VISIBLE) {
            mIfDrawText = false;
        }
        array.recycle();
    }

    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = measureHeight(heightMeasureSpec);
        setMeasuredDimension(width, height);

        mRealWidth = getMeasuredWidth() - getPaddingRight() - getPaddingLeft();
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        canvas.save();
        canvas.translate(getPaddingLeft(), getHeight() / 2);
        boolean noNeedBg = false;
        float radio = getProgress() * 1.0f / getMax();
        float progressPosX = (int) (mRealWidth * radio);
        String text = getProgress() + "%";
        // mPaint.getTextBounds(text, 0, text.length(), mTextBound);

        float textWidth = mPaint.measureText(text);
        float textHeight = (mPaint.descent() + mPaint.ascent()) / 2;

        if (progressPosX + textWidth > mRealWidth) {
            progressPosX = mRealWidth - textWidth;
            noNeedBg = true;
        }

        // draw reached bar
        float endX = progressPosX - mTextOffset / 2;
        if (endX > 0) {
            mPaint.setColor(mReachedBarColor);
            mPaint.setStrokeWidth(mReachedBarHeight);
            canvas.drawLine(0, 0, endX, 0, mPaint);
        }

        // draw progress bar
        // measure text bound
        if (mIfDrawText) {
            mPaint.setColor(mTextColor);
            canvas.drawText(text, progressPosX, -textHeight, mPaint);
        }
        // draw unreached bar
        if (!noNeedBg) {
            float start = progressPosX + mTextOffset / 2 + textWidth;
            mPaint.setColor(mUnReachedBarColor);
            mPaint.setStrokeWidth(mUnReachedBarHeight);
            canvas.drawLine(start, 0, mRealWidth, 0, mPaint);
        }

        canvas.restore();
    }

    private int measureHeight(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            float textHeight = mPaint.descent() - mPaint.ascent();
            result = (int) (getPaddingTop() + getPaddingBottom() +
                    Math.max(Math.max(mReachedBarHeight, mUnReachedBarHeight), Math.abs(textHeight)));
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    private int dp2px(int dpValue) {
        float value = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, getResources().getDisplayMetrics());
        return (int) value;
    }

    private int sp2px(int spValue) {
        float value = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue, getResources().getDisplayMetrics());
        return (int) value;
    }


}
