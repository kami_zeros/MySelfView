package com.zqq.horizontalprogress.withnumber;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.zqq.circleprogress.R;

public class Main4Activity extends AppCompatActivity {

    private RoundProgressBarWidthNumber mRoundProgressBar;
    private HProgressBarWithNumber mProgressBar;
    private HProgressBarNumber mProgressBar3;

    private static final int MSG_PROGRESS_UPDATE = 0x110;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int progress = mProgressBar.getProgress();
            int roundProgress = mRoundProgressBar.getProgress();
            int progress3 = mProgressBar3.getProgress();

            mProgressBar.setProgress(++progress);
            mRoundProgressBar.setProgress(++roundProgress);
            mProgressBar3.setProgress(++progress3);

            if (progress >= 100) {
                mHandler.removeMessages(MSG_PROGRESS_UPDATE);
            }
            mHandler.sendEmptyMessageDelayed(MSG_PROGRESS_UPDATE, 100);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        mProgressBar = findViewById(R.id.id_progressbar01);
        mRoundProgressBar = findViewById(R.id.id_progress02);
        mProgressBar3 = findViewById(R.id.progress_bar3);

        mHandler.sendEmptyMessage(MSG_PROGRESS_UPDATE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            mProgressBar.setProgress(0);
            mRoundProgressBar.setProgress(0);
            mProgressBar3.setProgress(0);

            mHandler.sendEmptyMessage(MSG_PROGRESS_UPDATE);
        }
        return super.onOptionsItemSelected(item);
    }

}
