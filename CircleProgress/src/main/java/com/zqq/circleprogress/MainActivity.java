package com.zqq.circleprogress;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.zqq.circleprogress.widget.CircularProgressBar;
import com.zqq.circleprogress.widget.RateTextCircularProgressBar;

public class MainActivity extends AppCompatActivity {

    private CircularProgressBar mCircularProgressBar;
    private RateTextCircularProgressBar mRateTextCircularProgressBar;

    private int progress = 0;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            mCircularProgressBar.setProgress(msg.what);
            mRateTextCircularProgressBar.setProgress(msg.what);
            if( progress >= 100 ) {
                progress = 0;
            }
            mHandler.sendEmptyMessageDelayed(progress++, 100);
            super.handleMessage(msg);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCircularProgressBar = (CircularProgressBar)findViewById(R.id.circular_progress_bar);
        mCircularProgressBar.setMax(100);
        mCircularProgressBar.setCircleWidth(20);
        mCircularProgressBar.setProgress(50);

        mRateTextCircularProgressBar = (RateTextCircularProgressBar)findViewById(R.id.rate_progress_bar);
        mRateTextCircularProgressBar.setMax(100);
        mRateTextCircularProgressBar.getCircularProgressBar().setCircleWidth(20);
        mRateTextCircularProgressBar.setProgress(50);
//        mHandler.sendEmptyMessageDelayed(progress++, 100);

    }
}
